package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.calculatePi;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PiCalculationMethodsTest {
  @Test
  void testGregoryLeibnitzSeries() {
    assertTrue(Math.abs(PiCalculationMethods.gregoryLeibnitzSeries() - Math.PI) < 0.01);
  }

  @Test
  void testNilakanthaSeries() {
    assertTrue(Math.abs(PiCalculationMethods.nilakanthaSeries() - Math.PI) < 0.0001);
  }

  @Test
  void testLimit() {
    assertTrue(Math.abs(PiCalculationMethods.limit() - Math.PI) < 0.0001);
  }

  @Test
  void testInverseSine() {
    assertTrue(Math.abs(PiCalculationMethods.inverseSine() - Math.PI) < 0.0001);
  }
}