package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils;

import java.net.InetAddress;
import java.util.Arrays;

public class InetAddressUtils {

    public static int compareAddresses(InetAddress addressA, InetAddress addressB) {
        byte[] addressABytes = addressA.getAddress();
        byte[] addressBBytes = addressB.getAddress();

        return Arrays.compare(addressABytes, addressBBytes);
    }

    public static int compareAddresses(InetAddress addressA, InetAddress addressB, int portA, int portB) {
        int addrRes = compareAddresses(addressA, addressB);
        if(addrRes != 0) {
            return addrRes;
        }
        return Integer.compare(portA, portB);
    }
}
