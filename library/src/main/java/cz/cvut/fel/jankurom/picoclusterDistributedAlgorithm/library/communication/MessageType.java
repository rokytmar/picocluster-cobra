package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication;

/**
 * Message type definition.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public enum MessageType {
  /**
   * Node requests server to assign UUID.
   */
  UUID_REQUEST,
  /**
   * Server confirms node's UUID.
   */
  UUID_CONFIRM,
  /**
   * Server tells nodes to start task.
   */
  START,
  /**
   * Node tells server that he finished task.
   */
  FINISHED,
  /**
   * Something bad happened and we want to notify the other side.
   */
  ERROR,
  /**
   * Server telling nodes to stop computation.
   */
  STOP,
  /**
   * Node failed the task.
   */
  FAILED,
  /**
   * Server sends client information about all ready clients.
   */
  NODES,
  /**
   * Nodes informs server about current status of the computation.
   */
  STATUS,
  /**
   * Send by client to the server when the client stopped the algorithm.
   */
  STOPPED,
  /**
   * Used by client to inform server he had sent a message.
   */
  SENT,
  /**
   * Sent by server to start an agent on client's side, assigning an id
   */
  COBRA_START_AGENT,
  /**
   * Sent by client to server, when the COBRA task initialization succeeded
   */
  COBRA_AGENT_INITIALIZED,
  /**
   * Sent by client to other clients to inform them that it finished all its tasks
   */
  COBRA_AGENT_FINISHED,
  /**
   * Sent by client to update server's copy of agent's trajectory - currently unused as token is passed by TCP via server
   */
  COBRA_NEW_TRAJECTORY,
  /**
   * Sent by cobra client to server or by server to all clients with updated state of token
   */
  COBRA_TOKEN,
  /**
   * Sent by client to other clients when demanding token
   */
  COBRA_TOKEN_REQUEST,
  /**
   * Sent by server to client to start the ADPP agent
   */
  ADPP_START_AGENT,
  /**
   * Sent by client to server when the ADPP task initialization succeeded
   */
  ADPP_AGENT_INITIALIZED,
  /**
   * Sent between nodes with encapsulated ADPP messages
   */
  ADPP_INNER_MESSAGE;
}