package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agentExperiments;

import java.io.File;
import java.util.Map;

/**
 * ADPP experiment class allowing it to be launched as algorithm.
 */
public class COBRAExperiment extends AbstractAgentExperiment {
    public COBRAExperiment() {
        this.experimentName = "COBRA Experiment";
    }

    @Override
    public String description() {
        return "This algorithms does not start one algorithm on nodes, but starts sequentially all pre-generated COBRA instances and collects the results";
    }
}
