package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.blinker;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Algorithm that displays patterns on the LEDs.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Blinker extends AbstractAlgorithm {

  private TaskRunner runner;
  private Patterns pattern;

  @Override
  public void receivedMessage(String uuid, String content) {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {

    runner = taskRunner;

    var duration = Integer.parseInt(parameters[0]);
    var pattern = Integer.parseInt(parameters[1]);

    if (pattern == 0) {
      Random random = new Random();
      pattern = random.nextInt(4) + 1;
    }
    int finalPattern = pattern;
    this.pattern = new Patterns();
    var thread = new Thread(() -> this.pattern.runPattern(finalPattern, taskRunner));
    thread.start();
    TimeUnit.SECONDS.sleep(duration);
    this.pattern.stop();
    while (this.pattern.isStopped()) {
      //  waiting for pattern to stop.
    }

    return "I am done.";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "All nodes have blinked.";
  }

  @Override
  public String name() {
    return "Blinker";
  }

  @Override
  public String description() {
    return "Task that is blinking LEDs in certain pattern. [0] - random pattern, [1] - RGB snake";
  }

  @Override
  public String[] parameters() {
    return new String[]{"Duration (in seconds)", "Pattern to blink"};
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public void stop() {
    pattern.stop();
    while (!pattern.isStopped()) {

    }
    runner.taskStopped();
  }
}