package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.StartRemoteAgent;

import java.io.Serializable;

/**
 * This message is passed from server to the node, to start the agent.
 */
public class StartRemoteCobraAgent extends StartRemoteAgent implements Serializable {
  /**
   * Pre-generated timestamp specifying when the agent should start his first task.
   */
  private int firstTaskAt;

  public StartRemoteCobraAgent(int id, int tickLength, long clockStartedAt, long initializedAt, int firstTaskAt) {
    super(id, tickLength, clockStartedAt, initializedAt);
    this.firstTaskAt = firstTaskAt;
  }

  public int getFirstTaskAt() {
    return firstTaskAt;
  }

  public void setFirstTaskAt(int firstTaskAt) {
    this.firstTaskAt = firstTaskAt;
  }
}
