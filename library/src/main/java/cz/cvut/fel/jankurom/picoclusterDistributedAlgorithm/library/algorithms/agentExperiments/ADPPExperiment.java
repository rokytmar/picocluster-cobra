package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agentExperiments;

import java.io.File;
import java.util.Map;

/**
 * ADPP experimnt class allowing it to be launched as algorithm.
 */
public class ADPPExperiment extends AbstractAgentExperiment{

    public ADPPExperiment() {
        this.experimentName = "ADPP Experiment";
    }

    @Override
    public String description() {
        return "This algorithms does not start one algorithm on nodes, but starts sequentially all pre-generated ADPP instances and collects the results";
    }
}
