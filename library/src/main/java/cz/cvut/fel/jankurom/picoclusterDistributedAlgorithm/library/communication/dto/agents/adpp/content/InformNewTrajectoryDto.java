package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content;


import cz.agents.admap.msg.InformNewTrajectory;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.MovingCircleDto;
import tt.euclid2i.trajectory.BasicSegmentedTrajectory;
import tt.euclidtime3i.Region;
import tt.euclidtime3i.region.CircleMovingToTarget;

import java.io.Serializable;

public class InformNewTrajectoryDto implements ContentDto {
    private String agentName;
    private MovingCircleDto movingCircleDto;

    public InformNewTrajectoryDto() {

    }

    public InformNewTrajectoryDto(InformNewTrajectory informNewTrajectory) {
        CircleMovingToTarget circ = (CircleMovingToTarget) informNewTrajectory.getRegion();
        movingCircleDto = new MovingCircleDto((BasicSegmentedTrajectory) circ.getTrajectory(), circ.getRadius(), circ.getSamplingInterval(), circ.getTargetReachedTime());
        agentName = informNewTrajectory.getAgentName();
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public MovingCircleDto getMovingCircleDto() {
        return movingCircleDto;
    }

    public void setMovingCircleDto(MovingCircleDto movingCircleDto) {
        this.movingCircleDto = movingCircleDto;
    }

    public Region getTrajectoryAsRegion() {
        return new CircleMovingToTarget(movingCircleDto.getTrajectory(), movingCircleDto.getRadius(), movingCircleDto.getTargetReachedTime());
    }
}
