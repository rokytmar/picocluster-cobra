package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.calculatePi;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class CalculatePi extends AbstractAlgorithm {

  private final Random random = new Random();

  @Override
  public void receivedMessage(String uuid, String content) {

  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) {
    switch (random.nextInt(5)) {
      case 0:
        return Double.toString(PiCalculationMethods.javaMathPi());
      case 1:
        return Double.toString(PiCalculationMethods.gregoryLeibnitzSeries());
      case 2:
        return Double.toString(PiCalculationMethods.nilakanthaSeries());
      case 3:
        return Double.toString(PiCalculationMethods.limit());
      case 4:
        return Double.toString(PiCalculationMethods.inverseSine());
    }
    return "0";
  }

  @Override
  public String composeResult(String[] partialResults) {
    var doubles = new ArrayList<Double>();
    for (String partialResult : partialResults) {
      doubles.add(Double.parseDouble(partialResult));
    }
    var sb = new StringBuilder();

    var mean = doubles.stream().reduce(0D, Double::sum) / doubles.size();
    sb.append("Mean:\t ").append(mean).append("\n");

    sb.append("Max:\t ").append(doubles.stream().max(Double::compareTo)).append("\n");
    sb.append("Min:\t ").append(doubles.stream().min(Double::compareTo)).append("\n");

    return sb.toString();
  }

  @Override
  public String name() {
    return "Calculate Pi";
  }

  @Override
  public String description() {
    return "Node calculate Pi using random method and server then computes some statistics over the results. Takes no parameters.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[0];
  }
}