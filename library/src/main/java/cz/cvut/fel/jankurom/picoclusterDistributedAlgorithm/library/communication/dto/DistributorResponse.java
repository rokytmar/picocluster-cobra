package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto;

/**
 * DTO for the response of distributor.
 */
public class DistributorResponse {
  private boolean error;
  private String data;

  public DistributorResponse(boolean error, String data) {
    this.error = error;
    this.data = data;
  }

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }
}