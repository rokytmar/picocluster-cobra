package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content.ContentDto;

import java.io.Serializable;
import java.util.Collection;

public class InnerADPPMessageDto implements Serializable {
    public enum MessageType {
        INFORM_NEW_TRAJECTORY,
        INFORM_AGENT_FINISHED,
        INFORM_AGENT_FAILED,
        INFORM_SUCCESSFUL_CONVERGENCE;
    }

    private String sender;
    private long id;
    private Collection<String> receivers;
    private MessageType messageType;

    private ContentDto contentDto;

    public InnerADPPMessageDto() {}

    public InnerADPPMessageDto(String sender, long id, Collection<String> receivers, MessageType messageType, ContentDto contentDto) {
        this.sender = sender;
        this.id = id;
        this.receivers = receivers;
        this.messageType = messageType;
        this.contentDto = contentDto;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Collection<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(Collection<String> receivers) {
        this.receivers = receivers;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public ContentDto getContentDto() {
        return contentDto;
    }

    public void setContentDto(ContentDto contentDto) {
        this.contentDto = contentDto;
    }
}
