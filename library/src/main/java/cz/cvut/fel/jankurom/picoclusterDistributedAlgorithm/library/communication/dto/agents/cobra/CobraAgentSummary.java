package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.AgentSummary;

/**
 * Message send by node to the server at the end of the task, containing all the info gethered by the agent.
 * This message is serialized and passed as a part of the partial result after the task ends
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraAgentSummary implements AgentSummary {
  private long baseSum;
  private long baseSumSq;
  private long waitSum;
  private long waitSumSq;
  private long planSum;
  private long planSumSq;
  private long pWindowSum;
  private long pWindowSumSq;
  private long prolongTSum;
  private long prolongTSumSq;
  private long prolongRSum;
  private long prolongRSumSq;
  private int taskCount;
  private long initializedAt;

  public CobraAgentSummary(long baseSum,
                           long baseSumSq,
                           long waitSum,
                           long waitSumSq,
                           long planSum,
                           long planSumSq,
                           long pWindowSum,
                           long pWindowSumSq,
                           long prolongTSum,
                           long prolongTSumSq,
                           long prolongRSum,
                           long prolongRSumSq,
                           int taskCount, long initializedAt) {
    this.baseSum = baseSum;
    this.baseSumSq = baseSumSq;
    this.waitSum = waitSum;
    this.waitSumSq = waitSumSq;
    this.planSum = planSum;
    this.planSumSq = planSumSq;
    this.pWindowSum = pWindowSum;
    this.pWindowSumSq = pWindowSumSq;
    this.prolongTSum = prolongTSum;
    this.prolongTSumSq = prolongTSumSq;
    this.prolongRSum = prolongRSum;
    this.prolongRSumSq = prolongRSumSq;
    this.taskCount = taskCount;
    this.initializedAt = initializedAt;
  }

  public long getInitializedAt() {
    return initializedAt;
  }

  public void setInitializedAt(long initializedAt) {
    this.initializedAt = initializedAt;
  }

  public int getTaskCount() {
    return taskCount;
  }

  public void setTaskCount(int taskCount) {
    this.taskCount = taskCount;
  }

  public long getBaseSum() {
    return baseSum;
  }

  public void setBaseSum(long baseSum) {
    this.baseSum = baseSum;
  }

  public long getBaseSumSq() {
    return baseSumSq;
  }

  public void setBaseSumSq(long baseSumSq) {
    this.baseSumSq = baseSumSq;
  }

  public long getWaitSum() {
    return waitSum;
  }

  public void setWaitSum(long waitSum) {
    this.waitSum = waitSum;
  }

  public long getWaitSumSq() {
    return waitSumSq;
  }

  public void setWaitSumSq(long waitSumSq) {
    this.waitSumSq = waitSumSq;
  }

  public long getPlanSum() {
    return planSum;
  }

  public void setPlanSum(long planSum) {
    this.planSum = planSum;
  }

  public long getPlanSumSq() {
    return planSumSq;
  }

  public void setPlanSumSq(long planSumSq) {
    this.planSumSq = planSumSq;
  }

  public long getpWindowSum() {
    return pWindowSum;
  }

  public void setpWindowSum(long pWindowSum) {
    this.pWindowSum = pWindowSum;
  }

  public long getpWindowSumSq() {
    return pWindowSumSq;
  }

  public void setpWindowSumSq(long pWindowSumSq) {
    this.pWindowSumSq = pWindowSumSq;
  }

  public long getProlongTSum() {
    return prolongTSum;
  }

  public void setProlongTSum(long prolongTSum) {
    this.prolongTSum = prolongTSum;
  }

  public long getProlongTSumSq() {
    return prolongTSumSq;
  }

  public void setProlongTSumSq(long prolongTSumSq) {
    this.prolongTSumSq = prolongTSumSq;
  }

  public long getProlongRSum() {
    return prolongRSum;
  }

  public void setProlongRSum(long prolongRSum) {
    this.prolongRSum = prolongRSum;
  }

  public long getProlongRSumSq() {
    return prolongRSumSq;
  }

  public void setProlongRSumSq(long prolongRSumSq) {
    this.prolongRSumSq = prolongRSumSq;
  }
}
