package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;

import java.awt.*;

public class ADPPBlinkItHandler {
    private TaskRunner taskRunner;

    public ADPPBlinkItHandler(TaskRunner taskRunner) {
        this.taskRunner = taskRunner;
    }

    public void setAgentRunning() {
        taskRunner.setLed(7, Color.ORANGE);
        taskRunner.setLed(6, Color.ORANGE);
        taskRunner.setLed(5, Color.ORANGE);
        taskRunner.setLed(4, Color.ORANGE);
    }

    public void setAgentCreatedNewTrajectory(int trajCount) {
        // Cycles R G B colors for each new trajectory
        taskRunner.setLed(0, trajCount % 3 == 0 ? 255 : 0, (trajCount + 1) % 3 == 0 ? 255 : 0, (trajCount + 2) % 3 == 0 ? 255 : 0);
    }

    public void setAgentSucceeded() {
        taskRunner.setLed(7, Color.GREEN);
        taskRunner.setLed(6, Color.GREEN);
        taskRunner.setLed(5, Color.GREEN);
        taskRunner.setLed(4, Color.GREEN);
        taskRunner.setLed(3, Color.GREEN);
        taskRunner.setLed(2, Color.GREEN);
        taskRunner.setLed(1, Color.GREEN);
    }

    public void setAgentFailed() {
        taskRunner.setLed(7, Color.RED);
        taskRunner.setLed(6, Color.RED);
        taskRunner.setLed(5, Color.RED);
        taskRunner.setLed(4, Color.RED);
        taskRunner.setLed(3, Color.RED);
        taskRunner.setLed(2, Color.RED);
        taskRunner.setLed(1, Color.RED);
    }

    public void setAgentAchievedConvergence() {
        taskRunner.setLed(7, Color.BLUE);
        taskRunner.setLed(6, Color.BLUE);
        taskRunner.setLed(5, Color.BLUE);
        taskRunner.setLed(4, Color.BLUE);
        taskRunner.setLed(3, Color.BLUE);
        taskRunner.setLed(2, Color.BLUE);
        taskRunner.setLed(1, Color.BLUE);
    }
}
