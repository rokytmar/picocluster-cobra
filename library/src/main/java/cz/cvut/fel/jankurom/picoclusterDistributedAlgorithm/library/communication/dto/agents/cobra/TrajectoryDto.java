package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import java.io.Serializable;

/**
 * A DTO representing BasicSegmentedTrajectory in a simplified minimalistic manner for more effective serialization.
 * The trajectory itself is serialized as an int array of subsequent points in the trajectory, each represented by three consecutive integers which represent the x,y,z coordinates of said point
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class TrajectoryDto implements Serializable {
    private int[] waypoints;

    private double cost;

    private int duration;

    public TrajectoryDto(int[] waypoints, double cost, int duration) {
        this.waypoints = waypoints;
        this.cost = cost;
        this.duration = duration;
    }

    public int[] getWaypoints() {
        return waypoints;
    }

    public double getCost() {
        return cost;
    }

    public int getDuration() {
        return duration;
    }
}
