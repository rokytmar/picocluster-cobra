package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;

/**
 * Abstract class containing common functionalities for algorithms that use agents
 */
public abstract class AbstractAgentsAlgorithm extends AbstractAlgorithm {
    protected static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    protected TaskRunner taskRunner;
    protected final String[] argNames;

    protected final Gson gson = new Gson();

    protected AbstractAgentThread agentThread;

    protected volatile boolean running;
    protected volatile boolean notNeeded;

    protected volatile boolean hardStop;

    protected Queue<Message> inboxQueue;

    protected final Object queueLock = new Object();

    protected AbstractAgentsAlgorithm(String[] argNames) {
        this.argNames = argNames;
        this.running = false;
        this.notNeeded = false;
        this.hardStop = false;
        this.inboxQueue = new LinkedList<>();
    }

    @Override
    public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
        LOGGER.info("Starting " + name());
        this.taskRunner = taskRunner;
        return null;
    }

    @Override
    public void stop() {
        LOGGER.info("Stopping agent");
        hardStop = true;
        try {
            if(agentThread != null) {
                taskRunner.sendStatusUpdate("Stopping " + name() + " agent " + agentThread.getId());
                agentThread.stop();
                agentThread.join();
                taskRunner.sendStatusUpdate("Stopped " + name() + " agent " + agentThread.getId());
            } else {
                taskRunner.sendStatusUpdate("Stoppend " + name() + " task that did not yet produce an agent");
            }
            LOGGER.info("Agent stopped");
            taskRunner.taskStopped();
        } catch (InterruptedException e) {
            LOGGER.severe("Exception during agent stopping! " + e.getMessage());
        }
    }



    @Override
    public void receivedMessage(String uuid, String content) {
        try {
            // Parses a message to Message dto type and passes it to the agent thread
            Message message = gson.fromJson(content, Message.class);
            synchronized (queueLock) {
                if (agentThread != null) {
                    while (!inboxQueue.isEmpty()) {
                        Message queuedMessage = inboxQueue.poll();
                        agentThread.receivedMessage(queuedMessage.getFrom(), queuedMessage);
                    }
                    agentThread.receivedMessage(uuid, message);
                } else {
                    inboxQueue.add(message);
                }
            }
        } catch (Exception e) {
            LOGGER.severe(name() + " received message and doesnt know what to do with it!\n" + e.getStackTrace());
        }
    }

    @Override
    public void triggerMessageQueueProcessing() {
        synchronized (queueLock) {
            while (!inboxQueue.isEmpty()) {
                Message queuedMessage = inboxQueue.poll();
                agentThread.receivedMessage(queuedMessage.getFrom(), queuedMessage);
            }
        }
    }

    @Override
    public String[] parameters() {
        return argNames;
    }

    @Override
    public Map<String, File> getDistributedFiles() {
        return new HashMap<>();
    }

    public static String[] prepareArgsForNode(String[] args, String problemFileContent) {
        List<String> tempList = new ArrayList<>(Arrays.asList(args));
        tempList.add(problemFileContent);
        String[] newArgs = tempList.toArray(new String[tempList.size()]);
        return newArgs;
    }
}
