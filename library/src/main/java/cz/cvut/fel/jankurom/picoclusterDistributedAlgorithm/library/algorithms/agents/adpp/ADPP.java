package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp;

import com.google.gson.JsonSyntaxException;
import cz.agents.admap.creator.ScenarioCreator;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.AbstractAgentsAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.AgentSummary;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.ADPPAgentSummary;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.StartRemoteADPPAgent;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils.AliteArgumentsParser;
import tt.jointeuclid2ni.probleminstance.EarliestArrivalProblem;
import tt.jointeuclid2ni.solver.Parameters;

import java.util.*;

/**
 * ADPP algorithm class
 */
public class ADPP extends AbstractAgentsAlgorithm {
    private static final String[] argNames = new String[] {"problemfile", "maxtime", "timestep", "timeout", "showvis"};

    private EarliestArrivalProblem problem;
    private Parameters params;

    public ADPP() {
        super(argNames);
    }

    @Override
    public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
        super.run(parameters, taskRunner);
        List<String> tempParamsList = new ArrayList<>(Arrays.asList(parameters));
        String problemString = tempParamsList.remove(tempParamsList.size() - 1);

        ScenarioCreator.setProblemString(problemString);

        String[] trueParams = tempParamsList.toArray(new String[tempParamsList.size()]);

        params = ScenarioCreator.createFromArgs(trueParams);
        problem = ScenarioCreator.getProblem();

        taskRunner.sendStatusUpdate("ADPP Agent Started", MessageType.ADPP_AGENT_INITIALIZED);

        // Waiting for id, which is assigned by server
        while(!running) {
        }

        // If the id assigned to this agent was bigger than the number of agents needed for this problem
        if(notNeeded) {
            String notNeeded = "Agent was not needed for this problem";
            LOGGER.info(notNeeded);
            return notNeeded;
        }
        // Wait for the agent thread to stop
        agentThread.join();
        AgentSummary summary = agentThread.getSummary();
        return gson.toJson(summary);
    }

    @Override
    public String composeResult(String[] partialResults) {
        List<ADPPAgentSummary> summaries = new ArrayList<>();
        Arrays.stream(partialResults).forEach(partialResult -> {
            try {
                ADPPAgentSummary summary = gson.fromJson(partialResult, ADPPAgentSummary.class);
                summaries.add(summary);
            } catch (JsonSyntaxException e) {
                // This can happen, as an agent that had this COBRA task assigned, but was needed, wont return a serialized AgentSummary
                // Will ignore this partial result in computing the final result
            }
        });

        double totalCost = 0;
        int totalMessages = 0;
        int totalReplanning = 0;
        long timeToConvergence = 0;
        long planningTime = 0;

        boolean success = true;
        for (ADPPAgentSummary summary : summaries) {
            if(!summary.getStatusString().equals("SUCCESS")) {
                success = false;
            }
            totalCost += summary.getCost();
            totalMessages += summary.getMessagesSent();
            totalReplanning += summary.getTotalReplannings();
            timeToConvergence = Long.max(timeToConvergence, summary.getTimeToConvergenceMs());
            planningTime += summary.getTotalTimePlanning();
        }

        return (success ? String.format("%.2f", totalCost) : "inf") + ";" +
            (success ? "SUCCESS" : "FAIL") + ";" + timeToConvergence + ";"
            + timeToConvergence + ";" + totalMessages + ";" + 0 + ";" + summaries.get(0).getClusters() + ";" + totalReplanning + ";" + planningTime;
    }

    @Override
    public String name() {
        return "ADPP";
    }

    @Override
    public String description() {
        return "Multi-agent path-finding problem solver, using ADPP (Asynchronous Decentralized Priority Planning) algorithm . Each agent will start on separate node, with server taking care of visualization";
    }

    @Override
    public void receivedDTOMessage(Message message) {
        switch(message.getType()) {
            case ADPP_START_AGENT:
                StartRemoteADPPAgent startMessage = gson.fromJson(message.getContent(), StartRemoteADPPAgent.class);
                startAgent(startMessage.getId(), startMessage.getTickLength(), startMessage.getIdToUUID());
                break;
            case ADPP_INNER_MESSAGE:
                if(agentThread == null || !running) {
                    LOGGER.warning("ADPP inner message accepted but no agent thread is running!");
                } else {
                    agentThread.receivedMessage(message.getFrom(), message);
                }
                break;
        }
    }

    private void startAgent(int id, int tickLength, Map<Integer, UUID> idToUUID) {
        // If id of this agent is higher that number of agents required, this agent is not necessary and finishes task
        if(id < problem.nAgents()) {
            agentThread = new ADPPAgentThread(taskRunner, problem, params, id, tickLength, idToUUID);
            agentThread.start();
        } else {
            notNeeded = true;
        }
        running = true;
    }

    public static String[] prepareADPPArgs(String[] algoParams) {
        return AliteArgumentsParser.parsePicoArgsToAlite(algoParams, ADPPDefaultArguments.getDefaultArguments(), argNames);
    }

    public static String[] parseArgumentsForPicocluster(String args, boolean showVis) {
        return AliteArgumentsParser.parseAliteToPicoArgs(args, true, showVis);
    }
}
