package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.StartRemoteAgent;

import java.util.Map;
import java.util.UUID;

/**
 * This message is passed from server to the node, to start the agent.
 */
public class StartRemoteADPPAgent extends StartRemoteAgent {
    private Map<Integer, UUID> idToUUID;

    public StartRemoteADPPAgent(int id, int tickLength, Map<Integer, UUID> idToUUIDMap) {
        super(id, tickLength, 0, 0);
        this.idToUUID= idToUUIDMap;
    }

    public Map<Integer, UUID> getIdToUUID() {
        return idToUUID;
    }

    public void setIdToUUID(Map<Integer, UUID> idToUUID) {
       this.idToUUID = idToUUID;
    }
}
