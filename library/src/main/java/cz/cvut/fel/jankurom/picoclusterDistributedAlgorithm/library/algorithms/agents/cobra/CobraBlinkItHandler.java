package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;

import java.awt.*;
import java.util.Random;

/**
 * Class taking care of visual representation of the Cobra agent on the Blinkt! module
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraBlinkItHandler {
  private TaskRunner taskRunner;

  public CobraBlinkItHandler(TaskRunner taskRunner) {
    this.taskRunner = taskRunner;
  }

  public void setAgentRunning() {
    taskRunner.setLed(7, Color.BLUE);
    taskRunner.setLed(6, Color.BLUE);
    taskRunner.setLed(5, Color.BLUE);
    taskRunner.setLed(4, Color.BLUE);
  }

  public void setAgentAskedForToken() {
    taskRunner.setLed(0, Color.BLUE);
  }

  public void setAgentLostToken() {
    taskRunner.setLed(0, Color.RED);
  }

  public void setAgentReceivedToken() {
    taskRunner.setLed(0, Color.GREEN);
  }

  public void setAgentNewTrajectory() {
    // Change color randomly for every new trajectory
    Random rnd = new Random();
    taskRunner.setLed(1, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

    taskRunner.setLed(2, Color.BLACK);
    taskRunner.setLed(3, Color.BLACK);
  }

  public void setAgentFinished() {
    taskRunner.setLed(2, Color.GREEN);
    taskRunner.setLed(3, Color.GREEN);
  }

  public void setAgentFailedPlanning() {
    taskRunner.setLed(2, Color.RED);
    taskRunner.setLed(3, Color.RED);
  }
}
