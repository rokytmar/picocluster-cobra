package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import tt.euclid2i.trajectory.BasicSegmentedTrajectory;

import java.io.Serializable;

// This message is currently not used, as the trajectory is passed in the token
/**
 * This message is passed from node to server, to notify the server about a new trajectory
 */
public class NewTrajectoryNotification implements Serializable {
  private BasicSegmentedTrajectory trajectory;

  public NewTrajectoryNotification(BasicSegmentedTrajectory trajectory) {
    this.trajectory = trajectory;
  }

  public BasicSegmentedTrajectory getTrajectory() {
    return trajectory;
  }

  public void setTrajectory(BasicSegmentedTrajectory trajectory) {
    this.trajectory = trajectory;
  }
}
