package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.fail;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.Random;

/**
 * Algorithm with probability to fail after some time.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Fail extends AbstractAlgorithm {
  @Override
  public void receivedMessage(String uuid, String content) {

  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    Random random = new Random();
    if (random.nextDouble() < Double.parseDouble(parameters[0])) {
      throw new Exception("I have failed!");
    }
    return "I haven't failed.";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "None of " + partialResults.length + " have failed.";
  }

  @Override
  public String name() {
    return "Failing task";
  }

  @Override
  public String description() {
    return "Node has a probability to fail. Otherwise it finishes task successfully.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Probability to fail (float between 0 and 1)"};
  }
}