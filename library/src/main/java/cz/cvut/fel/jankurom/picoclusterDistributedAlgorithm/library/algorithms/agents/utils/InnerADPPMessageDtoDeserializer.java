package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.utils;

import com.google.gson.*;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.InnerADPPMessageDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content.*;

import java.lang.reflect.Type;
import java.util.Collection;

/**
 * Custom deserializer allowing correct deserialization of InnerADPPMessageDto class
 */
public class InnerADPPMessageDtoDeserializer implements JsonDeserializer<InnerADPPMessageDto> {
    @Override
    public InnerADPPMessageDto deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Gson gsonUtil = new Gson();
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        String sender = jsonObject.get("sender").getAsString();
        long id = jsonObject.get("id").getAsLong();
        Collection<String> receivers = gsonUtil.fromJson(jsonObject.get("receivers"), Collection.class);
        InnerADPPMessageDto.MessageType messageType = gsonUtil.fromJson(jsonObject.get("messageType"), InnerADPPMessageDto.MessageType.class);

        ContentDto contentDto;

        switch (messageType) {
            case INFORM_NEW_TRAJECTORY:
                contentDto = gsonUtil.fromJson(jsonObject.get("contentDto"), InformNewTrajectoryDto.class);
                break;
            case INFORM_AGENT_FINISHED:
                contentDto = gsonUtil.fromJson(jsonObject.get("contentDto"), InformAgentFinishedDto.class);
                break;
            case INFORM_AGENT_FAILED:
                contentDto = gsonUtil.fromJson(jsonObject.get("contentDto"), InformAgentFailedDto.class);
                break;
            case INFORM_SUCCESSFUL_CONVERGENCE:
                contentDto = gsonUtil.fromJson(jsonObject.get("contentDto"), InformSuccessfulConvergenceDto.class);
                break;
            default:
                throw new IllegalArgumentException("Unknown MessageType: " + messageType);
        }
        return new InnerADPPMessageDto(sender, id, receivers, messageType, contentDto);
    }
}
