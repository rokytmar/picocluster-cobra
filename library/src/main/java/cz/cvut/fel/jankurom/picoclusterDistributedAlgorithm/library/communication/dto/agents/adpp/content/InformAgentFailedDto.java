package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content;

import cz.agents.admap.msg.InformAgentFailed;

public class InformAgentFailedDto implements ContentDto {
    private String agentName;

    public InformAgentFailedDto(InformAgentFailed message) {
        this.agentName = message.getAgentName();
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
}
