package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.AgentSummary;

import java.util.logging.Logger;

/**
 * Shared functionalities of agent Threads
 */
public  abstract class AbstractAgentThread extends Thread{
    protected static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    protected final Gson gson;
    protected final TaskRunner taskRunner;

    /**
     * Id of the agent
     */
    protected final int id;
    /**
     * Length of the tick in ms
     */
    protected final int tickTime;
    /**
     * Time the instance was initialized by the server
     */
    protected final long initializedAt;

    protected volatile boolean running;
    protected volatile boolean stop;
    protected boolean agentCompleted;

    public AbstractAgentThread(TaskRunner taskRunner, int id, int tickTime, long initializedAt) {
        this.taskRunner = taskRunner;
        this.id = id;
        this.tickTime = tickTime;
        this.initializedAt = initializedAt;

        gson = new Gson();

        this.running = true;
        this.stop = false;
    }


    /**
     * Sets the stop flag to true to stop the loop
     */
    public void stopAgent() {
        synchronized (AbstractAgentThread.class) {
            this.stop = true;
        }
    }

    /**
     * Method which takes caref ot a message handling for the agent
     * @param uuid uuid of the sender
     * @param message parsed Message object
     */
    public abstract void receivedMessage(String uuid, Message message);

    @Override
    public long getId() {
        return id;
    }

    /**
     * Get formated agent summary
     * @return
     */
    public abstract AgentSummary getSummary();
}
