package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class providing static methods to parse Picocluster parameters to arguments for ADPP and COBRA and vice versa
 */
public class AliteArgumentsParser {
    /**
     * Parse picocluster parameters to arguments accepted by ADPP and COBRA
     * @param algoParams parameters passed into GUI
     * @param defaultArguments default arguments for the algorithm
     * @param argNames names of the algorithm's parameters
     * @return Arguments formatted for ADPP and COBRA
     */
    public static String[] parsePicoArgsToAlite(String[] algoParams, Map<String, String> defaultArguments, String[] argNames) {
        // Create a map from passed arguments as <argName>-<value> pair
        Map<String, String> passedArguments = new HashMap<>();
        for(int i = 0 ; i < argNames.length; i++) {
            // Do not apply empty string, use default
            if(algoParams[i].equals("")) {
                continue;
            }
            passedArguments.put(argNames[i], algoParams[i]);
        }
        Map<String, String> arguments = defaultArguments;
        arguments.putAll(passedArguments); // Update default arguments with passed ones

        // Remove arguments that have no value and are only determined by their presence
        if(arguments.get("showvis").equals("false")) {
            arguments.remove("showvis");
        }
        if(arguments.get("agentsRemote").equals("false")) {
            arguments.remove("agentsRemote");
        }

        // Create string of formated arduments with '-argName argvalue' format
        StringBuilder argsBuilder = new StringBuilder();
        arguments.forEach((key, val) -> {
            if(key.equals("showvis") || key.equals("agentsRemote")) {
                argsBuilder.append(String.format("-%s ", key)); // Arguments that are passed without value - either present or not
            } else {
                argsBuilder.append(String.format("-%s %s ", key, val));
            }
        });
        return argsBuilder.toString().split(" ");
    }

    /**
     * Parses ADPP or COBRA arguments to parameters recognized by picocluster, specifically for the experiment
     * @param aliteArgs algorithm arguments
     * @param isADPP flag wheter the algorith is ADPP
     * @param showVis flag wheter to show visualization
     * @return Picocluster parameters of the algorithm problem
     */
    public static String[] parseAliteToPicoArgs(String aliteArgs, boolean isADPP, boolean showVis) {
        List<String> aliteArgsList = new LinkedList<>(Arrays.asList(aliteArgs.split(" ")));
        // Remove first two and last n elements depending on the algorithm - getting rid of unscessary arguments from original input
        aliteArgsList = aliteArgsList.subList(2, isADPP ? aliteArgsList.size() - 3 : aliteArgsList.size() - 2);

        List<String> picoArgs = new ArrayList<>();
        String badPrefix = "instances/";
        for(int i = 1; i < aliteArgsList.size(); i += 2) {
            // Solve problems with file structure
            if(i == 1) {
                String badPathToFile = aliteArgsList.get(i);
                picoArgs.add(badPathToFile.substring(badPrefix.length()));
            } else {
                picoArgs.add(aliteArgsList.get(i));
            }
        }
        picoArgs.add(showVis ? "true" : "false");

        return picoArgs.toArray(new String[picoArgs.size()]);
    }
}
