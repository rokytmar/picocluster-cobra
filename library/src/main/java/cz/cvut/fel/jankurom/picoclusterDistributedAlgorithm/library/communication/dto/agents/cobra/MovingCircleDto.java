package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.utils.TrajectorySerializer;
import tt.euclid2i.trajectory.BasicSegmentedTrajectory;

import java.io.Serializable;

/**
 * This class serves as a helper for the serialization of trajectory into the TokenDto
 * It replaces the unserializable Radius class, providing the same data in three separate fields
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class MovingCircleDto implements Serializable {
  private TrajectoryDto trajectoryDto;
  private int radius;
  private int samplingInterval;

  private int targetReachedTime;

  public MovingCircleDto(BasicSegmentedTrajectory trajectory, int radius, int samplingInterval) {
    this(trajectory, radius, samplingInterval, 0);
  }

  public MovingCircleDto(BasicSegmentedTrajectory trajectory, int radius, int samplingInterval, int targetReachedTime) {
    this.trajectoryDto = TrajectorySerializer.serializeTrajectory(trajectory);
    this.radius = radius;
    this.samplingInterval = samplingInterval;
    this.targetReachedTime = targetReachedTime;
  }

    public BasicSegmentedTrajectory getTrajectory() {
    return TrajectorySerializer.deserializeTrajectory(trajectoryDto);
  }

  public void setTrajectory(BasicSegmentedTrajectory trajectory) {
    this.trajectoryDto = TrajectorySerializer.serializeTrajectory(trajectory);
  }

  public int getTargetReachedTime() {
    return targetReachedTime;
  }

  public void setTargetReachedTime(int targetReachedTime) {
    this.targetReachedTime = targetReachedTime;
  }

  public int getRadius() {
    return radius;
  }

  public void setRadius(int radius) {
    this.radius = radius;
  }

  public int getSamplingInterval() {
    return samplingInterval;
  }

  public void setSamplingInterval(int samplingInterval) {
    this.samplingInterval = samplingInterval;
  }
}
