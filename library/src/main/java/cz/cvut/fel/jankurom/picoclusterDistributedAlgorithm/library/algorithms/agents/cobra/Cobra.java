package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra;

import com.google.gson.JsonSyntaxException;
import cz.agents.map4rt.CommonTime;
import cz.agents.map4rt.ScenarioCreator;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.AbstractAgentsAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp.ADPP;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.AgentSummary;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.CobraAgentSummary;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.StartRemoteCobraAgent;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils.AliteArgumentsParser;
import tt.jointeuclid2ni.probleminstance.RelocationTaskCoordinationProblem;
import tt.jointeuclid2ni.solver.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.agents.map4rt.ScenarioCreator.avg;
import static cz.agents.map4rt.ScenarioCreator.sd;

/**
 * A Cobra algorithm class that takes care of all needed message passing, stopping starting and handling the COBRA agent
 * parse parameters and compute final result
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class Cobra extends AbstractAgentsAlgorithm {
  private static final String[] argNames = new String[] {"problemfile", "ntasks", "timestep", "maxtime", "timeout", "seed", "showvis"};

  private volatile RelocationTaskCoordinationProblem problem;
  private volatile Parameters params;

  public Cobra() {
    super(argNames);
  }

    @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    super.run(parameters, taskRunner);
    List<String> tempParamsList = new ArrayList<>(Arrays.asList(parameters));
    String problemString = tempParamsList.remove(tempParamsList.size() - 1);

    ScenarioCreator.setProblemString(problemString);

    String[] trueParams = tempParamsList.toArray(new String[tempParamsList.size()]);


    params = ScenarioCreator.createFromArgs(trueParams);
    problem = ScenarioCreator.getProblem();

    taskRunner.sendStatusUpdate("COBRA Agent Started", MessageType.COBRA_AGENT_INITIALIZED);

    // Waiting for id, which is assigned by server
    while(!running && !hardStop) {
    }
    if(hardStop) {
      return "Agent Stopped";
    }
    // If the id assigned to this agent was bigger than the number of agents needed for this problem
    if(notNeeded) {
      String notNeeded = "Agent was not needed for this problem";
      LOGGER.info(notNeeded);
      return notNeeded;
    }
    // Wait for the agent thread to stop
    agentThread.join();
    AgentSummary summary = agentThread.getSummary();
    return gson.toJson(summary);
  }

  @Override
  public String composeResult(String[] partialResults) {
    List<CobraAgentSummary> summaries = new ArrayList<>();
    Arrays.stream(partialResults).forEach(partialResult -> {
      try {
        CobraAgentSummary summary = gson.fromJson(partialResult, CobraAgentSummary.class);
        summaries.add(summary);
      } catch (JsonSyntaxException e) {
        // This can happen, as an agent that had this COBRA task assigned, but was needed, wont return a serialized AgentSummary
        // Will ignore this partial result in computing the final result
      }
    });
    int nTasks = summaries.get(0).getTaskCount();
    long initializedAt = summaries.get(0).getInitializedAt();

    long baseSum = 0;
    long baseSumSq = 0;
    long waitSum = 0;
    long waitSumSq = 0;
    long planSum = 0;
    long planSumSq = 0;
    long pWindowSum = 0;
    long pWindowSumSq = 0;
    long prolongTSum = 0;
    long prolongTSumSq = 0;
    long prolongRSum = 0;
    long prolongRSumSq = 0;

    for (CobraAgentSummary summary : summaries) {
      baseSum +=  summary.getBaseSum();
      baseSumSq += summary.getBaseSumSq();

      waitSum += summary.getWaitSum();
      waitSumSq += summary.getWaitSumSq();

      planSum += summary.getPlanSum();
      planSumSq += summary.getPlanSumSq();

      pWindowSum += summary.getpWindowSum();
      pWindowSumSq += summary.getpWindowSumSq();

      prolongTSum += summary.getProlongTSum();
      prolongTSumSq += summary.getProlongTSumSq();

      prolongRSum += summary.getProlongRSum();
      prolongRSumSq += summary.getProlongRSumSq();
    }

    long n = (long) summaries.size() * nTasks;

    long avgBase = avg(baseSum, n);
    long varBase = sd(baseSumSq, avgBase, n);

    long avgWait = avg(waitSum, n);
    long varWait = sd(waitSumSq, avgWait, n);

    long avgPlan = avg(planSum, n);
    long varPlan = sd(planSumSq, avgPlan, n);

    long avgPWindow = avg(pWindowSum, n);
    long varPWindow = sd(pWindowSumSq,  avgPWindow, n);

    long avgProlongT = avg(prolongTSum, n);
    long varProlongT = sd(prolongTSumSq, avgProlongT, n);

    long avgProlongR = avg(prolongRSum, n);
    long varProlongR = sd(prolongRSumSq, avgProlongR, n);

    return "SUCCESS;" + avgBase  + ";" + varBase
        + ";" + avgWait  + ";" + varWait
        + ";" + avgPlan  + ";" + varPlan
        + ";" + avgPWindow  + ";" + varPWindow
        + ";" + avgProlongT  + ";" + varProlongT
        + ";" + avgProlongR   + ";" + varProlongR
        + ";" + (CommonTime.currentTimeMs() - initializedAt);
  }

  @Override
  public String name() {
      return "COBRA";
  }

  @Override
  public String description() {
      return "Multi-agent path-finding problem solver, which uses COBRA algorithm (Continuous Best Response Algorithm). Each agent will start on separate node, with server taking care of visualization and helping with Token passing";
  }

  @Override
  public void receivedDTOMessage(Message message) {
    switch (message.getType()) {
      case COBRA_START_AGENT:
        StartRemoteCobraAgent startMessage = gson.fromJson(message.getContent(), StartRemoteCobraAgent.class);
        startAgent(startMessage.getId(), startMessage.getTickLength(), startMessage.getClockStartedAt(), startMessage.getInitializedAt(), startMessage.getFirstTaskAt());
        break;
      case COBRA_AGENT_FINISHED:
      case COBRA_TOKEN:
      case COBRA_TOKEN_REQUEST:
        agentThread.receivedMessage(message.getFrom(), message);
        break;
      default:
        LOGGER.warning("Cobra received an unknown message type");
    }
  }

  private void startAgent(int id, int tickLength, long serverClockStartedAt, long initializedAt, int firstTaskAt) {
    // If id of this agent is higher that number of agents required, this agent is not necessary and finishes task
    if(id < problem.nAgents()) {
      // Sync local clock to be identical with server
      CommonTime.syncTime(serverClockStartedAt);
      agentThread = new CobraAgentThread(taskRunner, problem, params, id, tickLength, initializedAt, firstTaskAt);
      agentThread.start();
    } else {
      notNeeded = true;
    }
    running = true;
  }

  /**
   * Parses algorithm parameters as passed by user into server's gui, to  a format required by cobra
   * @param algoParams parameters from gui
   * @return arguments for cobra
   */
  public static String[] prepareCobraArgs(String[] algoParams) {
    return AliteArgumentsParser.parsePicoArgsToAlite(algoParams, CobraDefaultArguments.getDefaultArguments(), argNames);
  }

  public static String[] parseArgumentsForPicocluster(String args, boolean showVis) {
    return AliteArgumentsParser.parseAliteToPicoArgs(args, false, showVis);
  }
}
