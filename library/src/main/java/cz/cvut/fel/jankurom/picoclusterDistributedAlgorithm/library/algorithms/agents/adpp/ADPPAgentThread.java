package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp;

import cz.agents.admap.agent.ADPPAgent;
import cz.agents.alite.communication.ExternalCommunicator;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.AbstractAgentThread;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.ADPPAgentSummary;
import tt.jointeuclid2ni.probleminstance.EarliestArrivalProblem;
import tt.jointeuclid2ni.solver.Parameters;

import java.util.*;
import java.util.stream.Collectors;

/**
 * ADPP agent thread class providing support and message handling for the Agent
 */
public class ADPPAgentThread extends AbstractAgentThread {
    /**
     * blinkt! module handler
     */
    private ADPPBlinkItHandler blinkItHandler;
    /**
     * The problem currently being solved
     */
    private EarliestArrivalProblem problem;
    /**
     * Parameters used fot current problem
     */
    private Parameters parameters;

    /**
     * An instance of the ADPP agent
     */
    private ADPPAgent agent;
    /**
     * External communicator, providing adpp communication layer for the agent
     */
    private final ExternalCommunicator communicator;

    /**
     * An adapter between ADPP's communicator and message layer, and Picocluster messaging layer
     */
    private ADPPCommunicatorAdapter communicatorAdapter;

    /**
     * All agent participating in current problem
     */
    private final Collection<Integer> allAgents;
    /**
     * Collection of agents that have not yet sent "AgentFinished" message
     */
    private final Collection<Integer> unfinishedAgents;

    private long timeStarted;

    private long timeFinished;

    public ADPPAgentThread(TaskRunner taskRunner, EarliestArrivalProblem problem, Parameters parameters,
                           int id, int tickTime, Map<Integer, UUID> idToUUID) {
        super(taskRunner, id, tickTime, 0);
        blinkItHandler = new ADPPBlinkItHandler(taskRunner);

        this.problem = problem;
        this.parameters = parameters;
        this.agent = new ADPPAgent(String.valueOf(id), problem.getStart(id), problem.getTarget(id),
            problem.getEnvironment(), problem.getBodyRadius(id), parameters.maxTime, parameters.timeStep, new LinkedList<>());

        // Create map only of nodes that will cooperate on this task actively
        Map<Integer, UUID> mapForCoworkers = new HashMap<>();
        for(int i = 0; i < problem.nAgents(); i++) {
            mapForCoworkers.put(i, idToUUID.get(i));
        }

        allAgents = mapForCoworkers.keySet();
        unfinishedAgents = mapForCoworkers.keySet();

        // Create a commucation adapter and use it to create external communicator
        this.communicatorAdapter = new ADPPCommunicatorAdapter(id, taskRunner, mapForCoworkers, agent);
        this.communicator = new ExternalCommunicator(communicatorAdapter, id);
        agent.setCommunicator(communicator,
            allAgents.stream().map(Object::toString).collect(Collectors.toList())
        );
        agent.setPlanningGraph(problem.getPlanningGraph());
    }

    @Override
    public void run() {
        running = true;
        taskRunner.sendStatusUpdate("Agent " + id + " has started");
        blinkItHandler.setAgentRunning();
        timeStarted = System.currentTimeMillis();
        agent.start();

        // Process messages before starting the loop if any messages came early
        taskRunner.triggerMessageQueueProcessing();
        while(!stop) {
            try {
                Thread.sleep(tickTime);
            } catch (InterruptedException e) {}

            agent.tick(System.nanoTime());
            // Process queue in every tick to be sure
            communicatorAdapter.processQueue();

            // End algorithm if globally terminated
            if (agent.isGlobalTerminationDetected()) {
                stop = true;
                if(agent.hasSucceeded()) {
                    blinkItHandler.setAgentSucceeded();
                } else {
                    blinkItHandler.setAgentFailed();
                }
            }
        }
        timeFinished = System.currentTimeMillis();
        LOGGER.info("Agent " + agent.getName() + " terminating");
        running = false;
    }

    public void receivedMessage(String uuid, Message message) {
        switch (message.getType()) {
            case ADPP_INNER_MESSAGE:
                communicatorAdapter.receive(agent, message);
                break;
            default:
                LOGGER.warning("ADPP thread received a message but does not know what to do with it. Message type: " + message.getType());
        }
    }

    @Override
    public ADPPAgentSummary getSummary() {
        return new ADPPAgentSummary(agent.hasSucceeded() ? "SUCCESS" : "FAIL", agent.getName(),
            timeFinished - timeStarted,
            parameters.noOfClusters,
            agent.getCurrentTrajectory() == null ? Integer.MAX_VALUE : agent.getCurrentTrajectory().getCost(),
            agent.getMessageSentCounter(),
            agent.replanningCounter, agent.getTotalTimePlanning());
    }
}
