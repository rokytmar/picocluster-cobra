package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra;

import cz.agents.map4rt.CommonTime;
import cz.agents.map4rt.agent.CurrentTasks;
import cz.agents.map4rt.agent.RemoteCOBRAAgent;
import cz.agents.map4rt.agent.Token;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.AbstractAgentThread;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.AgentFinishedNotification;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.CobraAgentSummary;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.TokenDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.TokenRequest;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;
import tt.euclid2i.Point;
import tt.euclid2i.trajectory.BasicSegmentedTrajectory;
import tt.jointeuclid2ni.probleminstance.RelocationTaskCoordinationProblem;
import tt.jointeuclid2ni.solver.Parameters;

import java.util.*;

/**
 * A Thread taking care of the agent and token passing
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraAgentThread extends AbstractAgentThread {
  private final CobraBlinkItHandler blinkItHandler;

  private final RelocationTaskCoordinationProblem problem;
  private final Parameters params;

  private final RemoteCOBRAAgent agent;

  /**
   * Timestamp assigned by server when the agent should start first task
   */
  private final int firstTaskAt;
  private boolean hasToken;
  /**
   * Boolean denoting whether the agent can release the token if it holds it. This is true after the agent succsefully plans a trajectory
   */
  private boolean canReleaseToken;
  /**
   * Map of registered Token requests , newest one for each agent
   */
  private Map<String, TokenRequest> tokenRequests;
  /**
   * Timestamp denoting the time this node asked for the token for the last time
   */
  private long tokenRequestedAt;

  /**
   * Set of agent ids from which this agent received AgentFinished notifications
   */
  private Set<Integer> finishedAgentsIds;

  /**
   * Boolean specifying whether the agent has already asked the server for the initial token
   * This is used at the start of the algorithm, when the token is not yet passing and held by the server
   */
  private boolean serverTokenRequestSent;

  public CobraAgentThread(TaskRunner taskRunner, RelocationTaskCoordinationProblem problem, Parameters params,
                          int id, int tickTime, long initializedAt, int firstTaskAt) {
    super(taskRunner, id, tickTime, initializedAt);
    LOGGER.info("Starting cobra thread");

    this.agentCompleted = false;
    this.finishedAgentsIds = new HashSet<>();

    this.problem = problem;
    this.params = params;
    this.firstTaskAt = firstTaskAt;
    this.serverTokenRequestSent = false;

    blinkItHandler = new CobraBlinkItHandler(taskRunner);

    tokenRequests = new HashMap();
    tokenRequestedAt = -1;
    hasToken = false;
    canReleaseToken = true;

    // Clear tasks and token in case there are residual values
    CurrentTasks.clearTasks();

    Token.setAllRegions(new HashMap<>());
    CurrentTasks.docks = List.of(problem.getDocks());
    agent = new RemoteCOBRAAgent(String.valueOf(id),
        problem.getStart(id),
        params.nTasks,
        problem.getEnvironment(),
        problem.getPlanningGraph(),
        problem.getBodyRadius(id),
        problem.getMaxSpeed(id),
        params.maxTime + (int)initializedAt, params.timeStep, params.random);
  }

  @Override
  public void run() {
    running = true;
    agent.setIssueFirstTaskAt(initializedAt + firstTaskAt);
    agent.start();
    blinkItHandler.setAgentRunning();
    while (!stop) {
      try {
        Thread.sleep(tickTime);
      } catch (InterruptedException e) {}

      agent.tick(CommonTime.currentTimeMs());
      synchronized (CobraAgentThread.class) {
        if(!agentCompleted) {
          // If agent instance decided it needs a token to start a new task
          if(agent.wantsToStartTask()) {
            //False to keep the token until the finishing is done
            canReleaseToken = false;

            // Tick returns true if agent needed token for this tick
            if(hasToken) {
              try {
                agent.createNewTask();
              } catch (RuntimeException e) {
                // The task creation might fail due to planning issues or timeout. The node will try again in the new tick
                LOGGER.severe("Failed to finish planning in time! Will try again");
                taskRunner.sendStatusUpdate("Agent " + id + " failed create new trajectory");
                blinkItHandler.setAgentFailedPlanning();
                continue;
              }
              blinkItHandler.setAgentNewTrajectory();
              taskRunner.sendStatusUpdate("Agent " + id + " created new trajectory. Ramining tasks: " + agent.getRemainingTasks());
              LOGGER.info("Agent was assigned a new trajectory");
              canReleaseToken = true;
              sendToken();
            } else {
              askForToken();
            }
          }
          if(agent.hasCompletedAllTasks()) {
            // Upon completing all tasks, the node sends a notification to all his coworkers
            taskRunner.sendStatusUpdate("Agent " + id + " completed all his tasks");
            LOGGER.info("Completed all tasks");
            sendAgentEndMessageToCoworkers();
            agentCompleted = true;
            blinkItHandler.setAgentFinished();
            // If all other agents finished as well, stop the loop
            if(finishedAgentsIds.size() == problem.nAgents() - 1) {
              stop = true;
            }
          }
        }
      }
    }
    running = false;
    LOGGER.info("Agent " + agent.getName() + " terminating");
  }

  /**
   * Sends a message to all coworkers, informing them about the agent ending its tasks
   */
  private void sendAgentEndMessageToCoworkers() {
    AgentFinishedNotification notification = new AgentFinishedNotification(id);
    Arrays.stream(taskRunner.getCoworkers()).forEach(c -> {
      try {
        Message m = new Message(String.valueOf(id), c, gson.toJson(notification), MessageType.COBRA_AGENT_FINISHED);
        taskRunner.sendMessageToNode(c, gson.toJson(m));
      } catch (TooBigMessage e) {
        LOGGER.severe("Message too big while asking for token");
      }
    });
  }

  /**
   * Sends a token request to all other agents
   */
  private void askForToken() {
    // Send the request only if it was not requested before. This is to ensure the loop wont ask for the token with each tick
    if(tokenRequestedAt == -1) {
      blinkItHandler.setAgentAskedForToken();
      tokenRequestedAt = CommonTime.currentTimeMs();
      taskRunner.sendStatusUpdate("Agent " + id + " asked for token");
      sendTokenRequests();
    }
  }

  private void sendTokenRequests() {
    sendTokenRequests(false);
  }

  /**
   * Sends a token request to all nodes
   * @param isNotification flag, true if the request serves as a notification about an acquisition of the token
   */
  private void sendTokenRequests(boolean isNotification) {
    LOGGER.info(isNotification ? "Notifying other servers about token" : "Asking for token");
    TokenRequest tokenRequest = new TokenRequest(id, tokenRequestedAt, isNotification);

    Arrays.stream(taskRunner.getCoworkers()).forEach(c -> {
      try {
        Message m = new Message(String.valueOf(id), c, gson.toJson(tokenRequest), MessageType.COBRA_TOKEN_REQUEST);
        taskRunner.sendMessageToNode(c, gson.toJson(m));
      } catch (TooBigMessage e) {
        LOGGER.severe("Message too big while asking for token");
      }
    });

    // If the node has not performed any task, it can be first to start and also asks the server for the first token
    if(!serverTokenRequestSent) {
      serverTokenRequestSent = true;
      taskRunner.sendStatusUpdate(gson.toJson(new TokenRequest(id, CommonTime.currentTimeMs(), false)), MessageType.COBRA_TOKEN_REQUEST);
    }
  }

  /**
   * Sends the token to next node
   * This is done by sending the TokenDTO message with a next holder id to the server, which then sends it to the next recipient
   */
  private void  sendToken() {
    int nextId = id;
    synchronized (TokenRequest.class) {
      if(canReleaseToken) {
        // Select a node which has the request with the earliest timestamp as the next receipent
        var nextHolder = tokenRequests.values().stream().min(Comparator.comparingLong(TokenRequest::getRequestedAt));
        if(nextHolder.isPresent()) {
          // loose the token to this node
          nextId = nextHolder.get().getRequesterId();
          hasToken = false;
        }
        // Create tokenDto from local token copy
        TokenDto tokenDto = TokenDto.createFromRegions(nextId, Token.getAllRegions(), agent.getRandom());
        // Sending token to server over TCP
        taskRunner.sendStatusUpdate("Agent " + id + " is sending token to agent " + nextId);
        taskRunner.sendStatusUpdate(gson.toJson(tokenDto), MessageType.COBRA_TOKEN);
      }
    }
    if(nextId != id) {
      blinkItHandler.setAgentLostToken();
    }
  }

  @Override
  public void receivedMessage(String uuid, Message message) {
    switch (message.getType()) {
      case COBRA_TOKEN:
        LOGGER.info("Received token update from " + uuid);
        registerTokenAssigned(message);
        break;
      case COBRA_TOKEN_REQUEST:
        LOGGER.info("Received token request from " + uuid);
        registerTokenRequest(uuid, message);
        break;
      case COBRA_AGENT_FINISHED:
        LOGGER.info("Received agent finished notification from " + uuid);
        registerAgentFinished(message);
        break;
    }
  }

  /**
   * Method called to handle a AgentFinishednotification
   * @param message from the node
   */
  private void registerAgentFinished(Message message) {
    AgentFinishedNotification notification = gson.fromJson(message.getContent(), AgentFinishedNotification.class);
    synchronized (CobraAgentThread.class) {
      finishedAgentsIds.add(notification.getAgentId());
      String finished = finishedAgentsIds.stream().map(String::valueOf).reduce((a, b) -> String.format("%s, %s", a, b)).get();
      LOGGER.info("Received end message from agent " + notification.getAgentId() + ". Currently finished: " + finished);
      if(finishedAgentsIds.size() == problem.nAgents() - 1 && agentCompleted) {
        stop = true;
      }
    }
  }

  /**
   * Called when the node received a token request from another node
   * @param senderUUID uuid of the node that asks for the token
   * @param message message
   */
  private void registerTokenRequest(String senderUUID, Message message) {
    synchronized (TokenRequest.class) {
      TokenRequest request = gson.fromJson(message.getContent(), TokenRequest.class);
      // If other node sent a notification that it now has token, ask for token
      if(request.isNotification()) {
        if(tokenRequestedAt != -1) {
          sendTokenRequests(); // Send token request only if I need it
        }
      } else {
        // Register someone's request. Always use latest request (might have old request time
        if(hasToken) {
          tokenRequests.put(senderUUID, request);
          if(canReleaseToken) {
            sendToken();
          }
        }
      }
    }
  }

  /**
   * Method called upon receiving the message about acquiring the token
   * @param message
   */
  private void registerTokenAssigned(Message message) {
    synchronized (TokenRequest.class) {
      TokenDto tokenDto = gson.fromJson(message.getContent(), TokenDto.class);
      if(tokenDto.getNextHolderId() == this.id) {
        blinkItHandler.setAgentReceivedToken();
        /* If already had token - was just reassigned. This should no longer be needed, as the server doesnt resend the
           token to the same agent anymore
         */

        if(hasToken) {
          return;
        }

        // Just received token
        sendTokenRequests(true); // Notify other servers that I have the token

        //Update local token with received data
        Token.setAllRegions(tokenDto.getAsRegions());

        //Update local random
        agent.setRandom(tokenDto.getRandom());

        // Update current tasks based on the trajectories received
        tokenDto.getTrajectories().forEach((id, mcDto) -> {
          BasicSegmentedTrajectory trajectory = mcDto.getTrajectory();

          Point trajectoryFinalPoint = trajectory.getSegments().get(trajectory.getSegments().size() - 1).getEnd().getPosition();
          for (Point dock : CurrentTasks.docks) {
            if(dock.equals(trajectoryFinalPoint)) {
              CurrentTasks.tryToRegisterTask(id, dock);
            }
          }
        });

        long waitDuration = CommonTime.currentTimeMs() - tokenRequestedAt;
        agent.waitSum += waitDuration;
        agent.waitSumSq += waitDuration * waitDuration;

        hasToken = true;
        canReleaseToken = false;
        // Omit requests with older request date than my own. New ones will arrive
        tokenRequests = updateRequests(tokenRequestedAt);
        tokenRequestedAt = -1;
        taskRunner.sendStatusUpdate("Agent " + id + " has token");
      }
    }
  }

  /**
   * Updates the token reqest stored for each node. This wil remove the requests older than the request made by this node
   * @param tokenRequestedAt the timestamp when this node asked for the token
   * @return updated copy of tokenRequests
   */
  private Map<String, TokenRequest> updateRequests(long tokenRequestedAt) {
    Map<String, TokenRequest> filteredRequests = new HashMap<>();
    tokenRequests.forEach((id, req) -> {
      if(req.getRequestedAt() > tokenRequestedAt) filteredRequests.put(id, req);
    });
    return filteredRequests;
  }

  public CobraAgentSummary getSummary() {
    return new CobraAgentSummary(agent.baseSum, agent.baseSumSq, agent.waitSum, agent.waitSumSq,
        agent.planSum, agent.planSumSq, agent.pWindowSum, agent.pWindowSumSq, agent.prolongTSum, agent.prolongTSumSq,
        agent.prolongRSum, agent.prolongRSumSq, params.nTasks, initializedAt);
  }

  public long getInitializedAt() {
    return initializedAt;
  }
}
