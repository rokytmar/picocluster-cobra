package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents;


import java.io.Serializable;

/**
 * This message is passed from server to the node, to start the agent.
 */
public abstract class StartRemoteAgent implements Serializable {
    /**
     * Id assigned to the agent
     */
    protected int id;
    /**
     * Length of the tick used in the simulation
     */
    protected int tickLength;
    /**
     * Timestamp when the server's simulation clock started, used to sync the node clock
     */
    protected long clockStartedAt;
    /**
     * Simulation clock timestamp when from which the agent should be initialized
     */
    protected long initializedAt;

    public StartRemoteAgent(int id, int tickLength, long clockStartedAt, long initializedAt) {
        this.id = id;
        this.tickLength = tickLength;
        this.clockStartedAt = clockStartedAt;
        this.initializedAt = initializedAt;
    }

    public long getClockStartedAt() {
        return clockStartedAt;
    }

    public void setClockStartedAt(long clockStartedAt) {
        this.clockStartedAt = clockStartedAt;
    }

    public long getInitializedAt() {
        return initializedAt;
    }

    public void setInitializedAt(long initializedAt) {
        this.initializedAt = initializedAt;
    }

    public int getTickLength() {
        return tickLength;
    }

    public void setTickLength(int tickLength) {
        this.tickLength = tickLength;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
