package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.intervalMessage;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Algorithm for measuring the throughput of network.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class IntervalMessage extends AbstractAlgorithm {

  private volatile int received = 0;

  @Override
  public void receivedMessage(String uuid, String content) {
    synchronized (this) {
      received++;
    }
  }

  @Override
  public void stop() {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    int total = Integer.parseInt(parameters[1]) * taskRunner.getCoworkers().length;

    var sb = new StringBuilder();
    for (int i = 0; i < Integer.parseInt(parameters[2]); i++) {
      sb.append('A');
    }

    TimeUnit.MILLISECONDS.sleep(2000);

    var message = sb.toString();

    for (int i = 0; i < Integer.parseInt(parameters[1]); i++) {
      for (String coworker : taskRunner.getCoworkers()) {
        taskRunner.sendMessageToNode(coworker, message);
      }
      TimeUnit.MILLISECONDS.sleep(Integer.parseInt(parameters[0]));
    }

    TimeUnit.MILLISECONDS.sleep(10000);
    return new StringBuilder().append(received).append('/').append(total).toString();
  }

  @Override
  public String composeResult(String[] partialResults) {
    int total = 0, received = 0;

    for (var res : partialResults) {
      var parts = res.split("/");
      received += Integer.parseInt(parts[0]);
      total += Integer.parseInt(parts[1]);
    }

    return new StringBuilder().append(received).append('/').append(total).append(' ').append((received * 100) / total).append("%").toString();
  }

  @Override
  public String name() {
    return "Interval message measurement";
  }

  @Override
  public String description() {
    return "Nodes sent messages between them and count received messages. Node waits for 10 seconds after sending all messages. It waits for two seconds before sending images.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Delay between messages (ms)", "Number of messages", "Size of message content (Bytes)"};
  }
}