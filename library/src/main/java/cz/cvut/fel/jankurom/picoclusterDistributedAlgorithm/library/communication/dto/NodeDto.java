package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.UUID;

/**
 * DTO for distribution of information about the node.
 */
public class NodeDto implements Serializable {
  private InetAddress address;
  private int port;
  private int udpPort;
  private UUID uuid;

  public InetAddress getAddress() {
    return address;
  }

  public void setAddress(InetAddress address) {
    this.address = address;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }

  public UUID getUuid() {
    return uuid;
  }

  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  @Override
  public String toString() {
    return "[" + address.getHostAddress() + ":" + port + "/udp:" + udpPort + "]: " + uuid;
  }

  public int getUDPPort() {
    return udpPort;
  }

  public void setUdpPort(int udpPort) {
    this.udpPort = udpPort;
  }
}