package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.agents.admap.agent.ADPPAgent;
import cz.agents.admap.msg.InformAgentFailed;
import cz.agents.admap.msg.InformAgentFinished;
import cz.agents.admap.msg.InformNewTrajectory;
import cz.agents.admap.msg.InformSuccessfulConvergence;
import cz.agents.alite.communication.ExternalCommunicator;
import cz.agents.alite.communication.Message;
import cz.agents.alite.communication.MessageHandler;
import cz.agents.alite.communication.content.Content;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.utils.InnerADPPMessageDtoDeserializer;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.InnerADPPMessageDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content.*;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Communicator adapter serving as a adapter between the communication layers of Picocluster platform and ADPP's alite messaging
 */
public class ADPPCommunicatorAdapter implements MessageHandler {

    protected static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);


    /**
     * Gson with registered adapter allowing correct deserialization of InnerADPPMessageDto classes
     */
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(InnerADPPMessageDto.class, new InnerADPPMessageDtoDeserializer())
            .create();;

    /**
     * Id of this node
     */
    private int myId;
    /**
     * UUID of this node
     */
    private UUID myUUID;
    private TaskRunner taskRunner;
    /**
     * A map containing UUID for each agent based on their id
     */
    private Map<Integer, UUID> idToUUID;

    private ADPPAgent myAgent;

    /**
     * The inbox queue used to store incomming messages if the agent is not yet running
     */
    private Queue<Message> inboxQueue;

    private Object agentLock = new Object();

    private Object queueLock = new Object();

    public ADPPCommunicatorAdapter(int myId, TaskRunner taskRunner, Map<Integer, UUID> idToUUID, ADPPAgent myAgent) {
        this.myId = myId;
        this.taskRunner = taskRunner;
        this.idToUUID = idToUUID;
        this.myUUID = idToUUID.get(myId);
        this.myAgent = myAgent;
        this.inboxQueue = new LinkedList<>();
    }

    /**
     * Message called by ExternalCommunicator to notify this listener to send a new message
     * @param message Message to be sent
     */
    @Override
    public void notify(Message message) {
        // Prepare appropriate message dto for the passed message
        InnerADPPMessageDto.MessageType messageType = getTypeByMessage(message);
        ContentDto contentDto = createContentDtoForMessage(message, messageType);
        InnerADPPMessageDto messageDto = new InnerADPPMessageDto(message.getSender(), message.getId(),
            message.getReceivers(), messageType, contentDto);

        taskRunner.sendStatusUpdate("Agent " + myId + " sending new " + messageType);
        LOGGER.info("ADPP agent " + myId + " - Sending new " + messageType);
        handleLedStatus(messageType);

        // INform the server about new trajectory if the message is of type INFORM_NEW_TRAJECTORY
        if(messageType.equals(InnerADPPMessageDto.MessageType.INFORM_NEW_TRAJECTORY)) {
            sendNewTrajectoryToServer(messageDto);
        }

        // Send the message to all designated receivers
        message.getReceivers().forEach(receiver -> {
            UUID receiverUUID = idToUUID.get(Integer.parseInt(receiver));
            try {

                cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message picoMessage =
                    new cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message(
                        myUUID.toString(),
                        receiverUUID.toString(),
                        gson.toJson(messageDto),
                        MessageType.ADPP_INNER_MESSAGE
                    );

//                taskRunner.sendStatusUpdate("Agent " + myId + " sending new " + messageType + " to " + receiver);
                taskRunner.sendMessageToNode(receiverUUID.toString(), gson.toJson(picoMessage));
            } catch (TooBigMessage e) {
                LOGGER.severe("Message too big while sending message of ADPP! Size: " + gson.toJson(message).length());
            }
        });
    }

    /**
     * Inform a server about new trajectory
     * @param messageDto message dtowith serialized trajectory
     */
    private void sendNewTrajectoryToServer(InnerADPPMessageDto messageDto) {
        taskRunner.sendStatusUpdate(gson.toJson(messageDto), MessageType.ADPP_INNER_MESSAGE);
    }

    /**
     * Receives Picocluster message, parses it to ADPP message and passes to agent
     * @param agent agent which should receive the message
     * @param incommingMessage
     */
    public void receive(ADPPAgent agent, cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message incommingMessage) {
        // Create ADPP message from the incomming message
        InnerADPPMessageDto innerADPPMessageDto = gson.fromJson(incommingMessage.getContent(), InnerADPPMessageDto.class);
        Content content = createContentFromDto(innerADPPMessageDto.getContentDto(), innerADPPMessageDto.getMessageType());
        Message message = ExternalCommunicator.createMessageFromPicoMessage(innerADPPMessageDto.getSender(), content, innerADPPMessageDto.getId());

        LOGGER.info("ADPP agent " + myId + " received new " + innerADPPMessageDto.getMessageType() + " from " + innerADPPMessageDto.getSender());
//        taskRunner.sendStatusUpdate("Agent " + myId + " received new " + innerADPPMessageDto.getMessageType() + " from " + innerADPPMessageDto.getSender());

        // Try to pass the message to agent, or ad it to the queue if agent is not running
        try {
            if(agent.isAgentRunning()) {
                synchronized (agentLock) {
                    agent.notify(message);
                }
            } else {
                synchronized (queueLock) {
                    inboxQueue.add(message);
                }
            }
        } catch (Exception e) {
            LOGGER.severe("An exception occurred during ADPP agent parsing of new message: " + message);
            e.printStackTrace();
        }
    }

    /**
     * Pass all messages currently in queue to agent
     */
    public void processQueue() {
        if(!myAgent.isAgentRunning()) {
            LOGGER.info("Tried processing queue of size " + inboxQueue.size() + " but agent is not yet running");
            return;
        }
        synchronized (queueLock) {
            while(!inboxQueue.isEmpty()) {
                synchronized (agentLock) {
                    myAgent.notify(inboxQueue.poll());
                }
            }
        }
    }

    public int getQueueSize() {
        return inboxQueue.size();
    }

    /**
     * Create contentDto of a content for specified message type
     * @param message original message which content is to be parsed
     * @param messageType type of the message to have its content parsed
     * @return ContentDto created from content of the message
     */
    private static ContentDto createContentDtoForMessage(Message message, InnerADPPMessageDto.MessageType messageType) {
        switch (messageType) {
            case INFORM_NEW_TRAJECTORY:
                return new InformNewTrajectoryDto((InformNewTrajectory) message.getContent());
            case INFORM_AGENT_FINISHED:
                return new InformAgentFinishedDto((InformAgentFinished) message.getContent());
            case INFORM_AGENT_FAILED:
                return new InformAgentFailedDto((InformAgentFailed) message.getContent());
            case INFORM_SUCCESSFUL_CONVERGENCE:
                return new InformSuccessfulConvergenceDto();
            default:
                LOGGER.severe("ADPP communication adapter tried to create contentDTO but message type is unknown: " + messageType);
                return null;
        }
    }

    /**
     * Determines the message type based on the content of the given message.
     *
     * @param message the message to determine the type for
     * @return the message type based on the content, or null if the type is unknown
     */
    private static InnerADPPMessageDto.MessageType getTypeByMessage(Message message) {
        Content content = message.getContent();

        if(content instanceof InformNewTrajectory) {
            return InnerADPPMessageDto.MessageType.INFORM_NEW_TRAJECTORY;
        } else if (content instanceof InformAgentFinished) {
            return InnerADPPMessageDto.MessageType.INFORM_AGENT_FINISHED;
        } else if (content instanceof InformAgentFailed) {
            return InnerADPPMessageDto.MessageType.INFORM_AGENT_FAILED;
        } else if (content instanceof InformSuccessfulConvergence) {
            return InnerADPPMessageDto.MessageType.INFORM_SUCCESSFUL_CONVERGENCE;
        }
        return null;
    }

    /**
     * Creates content based on the provided content DTO and message type.
     *
     * @param contentDto the content DTO to create content from
     * @param messageType the message type to determine the content creation
     * @return the created content
     */
    private static Content createContentFromDto(ContentDto contentDto, InnerADPPMessageDto.MessageType messageType) {
        switch (messageType) {
            case INFORM_NEW_TRAJECTORY:
                InformNewTrajectoryDto trajDto = (InformNewTrajectoryDto) contentDto;
                return new InformNewTrajectory(trajDto.getAgentName(), trajDto.getTrajectoryAsRegion());
            case INFORM_AGENT_FINISHED:
                InformAgentFinishedDto agentFiDto = (InformAgentFinishedDto) contentDto;
                return new InformAgentFinished(agentFiDto.getAgentName());
            case INFORM_AGENT_FAILED:
                InformAgentFailedDto agentFaDto = (InformAgentFailedDto) contentDto;
                return new InformAgentFailed(agentFaDto.getAgentName());
            case INFORM_SUCCESSFUL_CONVERGENCE:
                return new InformSuccessfulConvergence();
            default:
                LOGGER.severe("ADPP communication adapter tried to create content but message type is unknown: " + messageType);
                return new Content(null);
        }
    }

    /**
     * Handles the LED status based on the provided message type.
     *
     * @param messageType the message type to handle the LED status for
     */
    private void handleLedStatus(InnerADPPMessageDto.MessageType messageType) {
        ADPPBlinkItHandler handler = new ADPPBlinkItHandler(taskRunner);
        switch (messageType) {
            case INFORM_NEW_TRAJECTORY:
                handler.setAgentCreatedNewTrajectory(myAgent.replanningCounter);
                break;
            case INFORM_AGENT_FINISHED:
                handler.setAgentSucceeded();
                break;
            case INFORM_AGENT_FAILED:
                handler.setAgentFailed();
                break;
            case INFORM_SUCCESSFUL_CONVERGENCE:
                handler.setAgentAchievedConvergence();
                break;
        }
    }
}
