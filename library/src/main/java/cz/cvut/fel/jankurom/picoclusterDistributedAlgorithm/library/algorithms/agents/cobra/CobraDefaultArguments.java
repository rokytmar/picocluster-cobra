package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra;

import java.util.HashMap;
import java.util.Map;

/**
 * Class containing the default arguments used for the COBRA algorithm
 * All the arguments can be edited here, but the GUI only allows some of them to be set by user
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraDefaultArguments {
  private static final Map<String, String> args;

  static {
    args = new HashMap<>();
    args.put("method", "COBRA"); // Changing this will really change nothing, as this project uses COBRA by default
    args.put("problemfile", "ubremen-r27/1.xml");
    args.put("ntasks", "4");
    args.put("timestep", "650");
    args.put("maxtime", "6000000");
    args.put("timeout", "6000000");
    args.put("seed", "8");
    args.put("showvis", "true");

    // This should not be changed, as it would triger the COBRA scenario creator to start the computations as normally, not on the nodes
    args.put("agentsRemote", "true");
  }

  public static Map<String, String> getDefaultArguments() {
    Map<String, String> arguments = new HashMap<>();
    CobraDefaultArguments.args.forEach(arguments::put);
    return arguments;
  }
}
