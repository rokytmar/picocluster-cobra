package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content;

import cz.agents.admap.msg.InformAgentFinished;

public class InformAgentFinishedDto implements ContentDto {
    private String agentName;

    public InformAgentFinishedDto(InformAgentFinished message) {
        this.agentName = message.getAgentName();
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }
}
