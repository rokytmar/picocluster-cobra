package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;

import java.io.File;
import java.util.Map;

/**
 * Defines unified interface for algorithms that are run using reflection.
 *
 * @author Roman Janků
 */
public abstract class AbstractAlgorithm {
  /**
   * Runs the algorithm with given parameters.
   *
   * @param parameters Parameters for the algorithm.
   */
  public abstract String run(String[] parameters, TaskRunner taskRunner) throws Exception;

  /**
   * Composes partial results returned by nodes to a one result of the distributed algorithm.
   *
   * @param partialResults Array of partial results returned by the nodes.
   * @return One composed result made out of partial results of the distributed algorithm.
   */
  public abstract String composeResult(String[] partialResults);

  /**
   * Returns name of the algorithm.
   *
   * @return Name of the algorithm.
   */
  public abstract String name();

  /**
   * Returns description of the algorithm.
   *
   * @return Description of the algorithm.
   */
  public abstract String description();

  /**
   * Returns description of parameters for the algorithm. Should return something like "[0] Integer: number of iterations; [2] String: ...".
   *
   * @return Description of parameters for the algorithm.
   */
  public abstract String[] parameters();

  /**
   * Called to stop the algorithm.
   */
  public abstract void stop();

  /**
   * Called when node receives UDP message from another node.
   *
   * @param uuid    UUID of the node sending the message.
   * @param content Content of the message.
   */
  public abstract void receivedMessage(String uuid, String content);

  /**
   * Called when node recieves message that the handler classified as a message with serialized DTO message
   * @param message
   */
  public void receivedDTOMessage(Message message) {};

  /**
   * Returns list of distributed files that will be loaded by server and will be available to clients to download.
   *
   * @return Hash map of string identification and file pointer pairs.
   */
  public abstract Map<String, File> getDistributedFiles();

  @Override
  public String toString() {
    return name();
  }


  /**
   * Triggers a message queue processing if the algorithm maintains any queue
   */
  public void triggerMessageQueueProcessing() {
  }
}