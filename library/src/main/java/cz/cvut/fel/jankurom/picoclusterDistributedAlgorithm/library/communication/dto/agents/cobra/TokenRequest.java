package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

/**
 * Message send by node to other nodes in order to ask for token or notify about token acquisition
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class TokenRequest {
  /**
   * Id of the node that requests the token, or just acquired the token
   */
  private int requesterId;
  /**
   * Timestamp when the agent requested the token
   */
  private long requestedAt;
  /**
   * Flag specifying whether this message is a token request or acquisition notification
   */
  private boolean isNotification;

  public TokenRequest(int requesterId, long requestedAt, boolean isNotification) {
    this.requesterId = requesterId;
    this.requestedAt = requestedAt;
    this.isNotification = isNotification;
  }

  public boolean isNotification() {
    return isNotification;
  }

  public void setNotification(boolean notification) {
    isNotification = notification;
  }

  public int getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(int requesterId) {
    this.requesterId = requesterId;
  }

  public long getRequestedAt() {
    return requestedAt;
  }

  public void setRequestedAt(long requestedAt) {
    this.requestedAt = requestedAt;
  }
}
