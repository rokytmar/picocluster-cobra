package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp;

import java.util.HashMap;
import java.util.Map;

public class ADPPDefaultArguments {

    private static final Map<String, String> args;

    static {
        args = new HashMap<>();
        args.put("method", "ADPP"); // Changing this will really change nothing, as this project uses ADPP by default
        args.put("problemfile", "ubremen-r27-docks/0.xml");
        args.put("timestep", "27");
        args.put("timeout", "15000");
        args.put("showvis", "true");
        args.put("maxtime", "15000");

        args.put("agentsRemote", "true");
    }

    public static Map<String, String> getDefaultArguments() {
        Map<String, String> arguments = new HashMap<>();
        ADPPDefaultArguments.args.forEach(arguments::put);
        return arguments;
    }
}
