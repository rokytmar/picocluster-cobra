package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import tt.euclid2i.trajectory.BasicSegmentedTrajectory;
import tt.euclidtime3i.Region;
import tt.euclidtime3i.region.MovingCircle;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Message passed by nodes and the server to pass the Token and sync planned trajectories
 * The token also includes a random to sync random accross nodes before any new decision
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class TokenDto implements Serializable {
  /**
   * Id of the agent to acquire the token next
   */
  private int nextHolderId;
  /**
   * All currently planned trajectories for each agent
   */
  private Map<String, MovingCircleDto> trajectories;

  private Random random;

  public TokenDto(int nextHolderId, Map<String, MovingCircleDto> trajectories, Random random) {
    this.nextHolderId = nextHolderId;
    this.trajectories = trajectories;
    this.random = random;
  }

  public Map<String, MovingCircleDto> getTrajectories() {
    return trajectories;
  }

  public void setTrajectories(Map<String, MovingCircleDto> trajectories) {
    this.trajectories = trajectories;
  }

  public int getNextHolderId() {
    return nextHolderId;
  }

  public void setNextHolderId(int nextHolderId) {
    this.nextHolderId = nextHolderId;
  }

  public Random getRandom() {
    return random;
  }

  public void setRandom(Random random) {
    this.random = random;
  }

  public Map<String, Region> getAsRegions() {
    Map<String, Region> regions = new HashMap<>();
    trajectories.forEach((id, mcDto) -> {
      MovingCircle circle = new MovingCircle(mcDto.getTrajectory(), mcDto.getRadius(), mcDto.getSamplingInterval());
      regions.put(id, circle);
    });
    return regions;
  }

  public static TokenDto createFromRegions(int nextHolderId, Map<String, Region> regs, Random random) {
    Map<String, MovingCircleDto> trajs = new HashMap<>();
    regs.forEach((id, reg) -> {
      MovingCircle c = (MovingCircle)reg;
      trajs.put(id, new MovingCircleDto((BasicSegmentedTrajectory) c.getTrajectory(), c.getRadius(), c.getSamplingInterval()));
    });
    return new TokenDto(nextHolderId, trajs, random);
  }
}
