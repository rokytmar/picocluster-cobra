package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra;

import java.io.Serializable;

/**
 * Message sent by node to other node, to inform them about a node ending its computations
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class AgentFinishedNotification implements Serializable {
  /**
   * Id of the agent that had finished and is sending the message
   */
  private int agentId;

  public AgentFinishedNotification(int agentId) {
    this.agentId = agentId;
  }

  public int getAgentId() {
    return agentId;
  }

  public void setAgentId(int agentId) {
    this.agentId = agentId;
  }
}
