package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agentExperiments;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;

import java.io.File;
import java.util.Map;

/**
 * Abstract class providing shared functionalities for agent experiments algorithms
 */
public abstract class AbstractAgentExperiment extends AbstractAlgorithm {
    protected String experimentName;

    @Override
    public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
        return "This algorithm should never run directly on nodes, " +
            "instead serving as a abstract task for the server to recognize the experiment," +
            "start the experiment director and start corresponding algorithms on nodes";
    }

    @Override
    public String name() {
        return experimentName;
    }

    public String composeResult(String[] partialResults) {
        return null;
    }

    @Override
    public void stop() {

    }

    @Override
    public void receivedMessage(String uuid, String content) {

    }

    @Override
    public void receivedDTOMessage(Message message) {
        super.receivedDTOMessage(message);
    }

    @Override
    public Map<String, File> getDistributedFiles() {
        return null;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String[] parameters() {
        return new String[]{"show vis"};
    }
}
