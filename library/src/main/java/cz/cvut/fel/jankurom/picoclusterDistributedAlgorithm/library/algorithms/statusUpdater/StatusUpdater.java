package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.statusUpdater;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Algorithm that sends given number of status updates to the server.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class StatusUpdater extends AbstractAlgorithm {
  @Override
  public void stop() {

  }

  @Override
  public void receivedMessage(String uuid, String content) {

  }

  @Override
  public String run(String[] parameters, TaskRunner taskRunner) throws Exception {
    var count = Integer.parseInt(parameters[0]);
    //  DELIBERATELY COUNTING FROM ONE!!! code is a bit more readable this way
    for (int i = 1; i <= count; i++) {
      taskRunner.sendStatusUpdate("Done: " + i + " out of " + count);
      TimeUnit.SECONDS.sleep(5);
    }
    return "I am done!";
  }

  @Override
  public String composeResult(String[] partialResults) {
    return "All nodes sends all status updates successfully.";
  }

  @Override
  public String name() {
    return "Status updater task";
  }

  @Override
  public String description() {
    return "Sends given number of status updates to the server then finishes.";
  }

  @Override
  public HashMap<String, File> getDistributedFiles() {
    return new HashMap<>();
  }

  @Override
  public String[] parameters() {
    return new String[]{"Number of status updates to send"};
  }
}