package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.utils;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.TrajectoryDto;
import tt.euclidtime3i.Point;
import tt.euclid2i.trajectory.BasicSegmentedTrajectory;
import tt.euclidtime3i.discretization.Straight;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Helper class providing a transition (serialization) between BasicSegmentedTrajectory and TrajectoryDTO
 */
public class TrajectorySerializer {
    /**
     * Serializes a BasicSegmentedTrajectory instance into a TrajectoryDTO
     * @param trajectory to be serialized
     * @return TrajectoryDto representing the original trajectory
     */
    public static TrajectoryDto serializeTrajectory(BasicSegmentedTrajectory trajectory) {
        List<Integer> waypoints = new ArrayList<>();

        // Add the begining point of the trajectory
        waypoints.addAll(getWaypointsFromPoint(trajectory.getSegments().get(0).getStart()));

        trajectory.getSegments().stream().forEach(segment -> {
            // Always add only the end point as the start point is always the same as last segment's end
            waypoints.addAll(getWaypointsFromPoint(segment.getEnd()));
        });

        return new TrajectoryDto(waypoints.stream().mapToInt(i -> i).toArray(), trajectory.getCost(), trajectory.getMaxTime() - trajectory.getMinTime());
    }

    /**
     * Deserializes BasicSegmentedTrajectory from TrajectoryDto
     * @param trajectoryDto to be deserialized
     * @return BasicSegmentedTrajectory instances representing passed trajectory
     */
    public static BasicSegmentedTrajectory deserializeTrajectory(TrajectoryDto trajectoryDto) {
        List<Straight> segments = new ArrayList<>();
        int[] waypoints = trajectoryDto.getWaypoints();

        Point start = null;
        Point end = null;

        for (int i = 0; i < waypoints.length;) {
            if(start == null) {
                start = new Point(waypoints[i++], waypoints[i++], waypoints[i++]);
            }
            end = new Point(waypoints[i++], waypoints[i++], waypoints[i++]);
            segments.add(new Straight(start, end));
            start = end;
        }

        return new BasicSegmentedTrajectory(segments, trajectoryDto.getDuration(), trajectoryDto.getCost());
    }

    private static List<Integer> getWaypointsFromPoint(Point point) {
        List<Integer> pointWaypoints = new LinkedList<>();
        pointWaypoints.add(point.getX());
        pointWaypoints.add(point.getY());
        pointWaypoints.add(point.getZ());
        return pointWaypoints;
    }
}
