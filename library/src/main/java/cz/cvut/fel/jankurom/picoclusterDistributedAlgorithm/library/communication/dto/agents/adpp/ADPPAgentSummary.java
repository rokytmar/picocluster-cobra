package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.AgentSummary;

public class ADPPAgentSummary implements AgentSummary {
    private String statusString;

    private String agentName;

    private long timeToConvergenceMs;

    private int clusters;

    private double cost;

    private int messagesSent;

    private int totalReplannings;

    private long totalTimePlanning;

    public ADPPAgentSummary(String statusString, String agentName, long timeToConvergenceMs, int clusters, double cost,
                            int messagesSent, int totalReplannings, long totalTimePlanning) {
        this.statusString = statusString;
        this.agentName = agentName;
        this.timeToConvergenceMs = timeToConvergenceMs;
        this.clusters = clusters;
        this.cost = cost;
        this.messagesSent = messagesSent;
        this.totalReplannings = totalReplannings;
        this.totalTimePlanning = totalTimePlanning;
    }

    public long getTotalTimePlanning() {
        return totalTimePlanning;
    }

    public void setTotalTimePlanning(long totalTimePlanning) {
        this.totalTimePlanning = totalTimePlanning;
    }

    public String getStatusString() {
        return statusString;
    }

    public void setStatusString(String statusString) {
        this.statusString = statusString;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public long getTimeToConvergenceMs() {
        return timeToConvergenceMs;
    }

    public void setTimeToConvergenceMs(long timeToConvergenceMs) {
        this.timeToConvergenceMs = timeToConvergenceMs;
    }

    public int getClusters() {
        return clusters;
    }

    public void setClusters(int clusters) {
        this.clusters = clusters;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getMessagesSent() {
        return messagesSent;
    }

    public void setMessagesSent(int messagesSent) {
        this.messagesSent = messagesSent;
    }

    public int getTotalReplannings() {
        return totalReplannings;
    }

    public void setTotalReplannings(int totalReplannings) {
        this.totalReplannings = totalReplannings;
    }
}
