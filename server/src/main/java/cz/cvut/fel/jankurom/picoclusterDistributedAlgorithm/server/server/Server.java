package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.AbstractAgentsAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp.ADPP;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra.Cobra;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.NodeDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.StartTask;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils.InetAddressUtils;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.Configuration;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.AlreadyHasUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.NoDataServerIsRunning;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.ReservedUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.UuidAlreadyInUse;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ClientListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ServerListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.distributor.Distributor;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.multiAgentExperiment.MultiAgentExperimentDirector;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors.ADPPDirector;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors.CobraDirector;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors.MultiAgentTaskDirector;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class is singleton.
 *
 * @author Roman Janků
 */

public final class Server extends Thread implements ClientListener {

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");
  /**
   * The only instance of this class as this is singleton.
   */
  private static Server instance = null;
  /**
   * List of clients.
   */
  private final List<Client> clients = new ArrayList<>();
  /**
   * List of listeners.
   */
  private final List<ServerListener> listeners = new ArrayList<>();
  /**
   * This is a lock that everyone must obtain when he wants to work with clients.
   */
  private final Object clientLock = new Object();
  /**
   * Timer for the client update task.
   */
  private final Timer timer = new Timer();
  /**
   * Gson for parsing JSON into Java objects and serializing Java objects into JSON.
   */
  private final Gson gson;
  /**
   * List of tasks.
   */
  private final List<Task> tasks;
  /**
   * Tells weather the server is running.
   */
  private boolean running = false;
  /**
   * Tells weather clients changed.
   */
  private volatile boolean newClients = false;
  /**
   * Index of the current task. Points one index above the last task in list if all tasks have finished (or failed).
   */
  private volatile int nextTask = 0;
  /**
   * Special subserver director that starts when a COBRA or ADPP task is created, and controls the communication with client agents
   */
  private MultiAgentTaskDirector multiAgentTaskDirector;

  private MultiAgentExperimentDirector experimentDirector;

  /**
   * Hide constructor because this is singleton.
   */
  private Server() {
    gson = new Gson();
    tasks = new ArrayList<>();

    try {
      Distributor.getDistributor().startDistributorServer();
    } catch (IOException ex) {
      // TODO
    }
  }

  /**
   * Get the only instance of this class.
   *
   * @return The only instance of this class.
   */
  public static Server getInstance() {
    if (instance == null) {
      synchronized (Server.class) {
        if (instance == null) {
          instance = new Server();
        }
      }
    }
    return instance;
  }

  @Override
  public void run() {

    LOGGER.info(() -> "Starting server ...");

    ServerSocket serverSocket;

    try {
      //  Start the timer for client updates.
      timer.schedule(new TimerTask() {
        @Override
        public void run() {
          updateClients();
        }
      }, 5000, 2000);

      //  Create the server socket.
      serverSocket = new ServerSocket(Integer.parseInt(Configuration.getValue("port")));
      listeners.forEach(ServerListener::serverStarted);
      running = true;

      LOGGER.info(() -> "Server started, listening for clients.");

      while (running) {
        try {
          Socket socket = serverSocket.accept();
          synchronized (clientLock) {
            Client client = new Client(socket);
            client.start();
            clients.add(client);
            client.addClientListener(this);
            listeners.forEach(l -> l.clientsChanged("New client " + client));
            LOGGER.info(() -> "New client from " + socket.getInetAddress() + ":" + socket.getPort() + " with UUID" +
                client.getUuid());
          }
        } catch (SocketTimeoutException ex) {
          //  This exception is thrown so the server can be stopped
          LOGGER.fine(() -> "Server socket timed out.");
        } catch (IOException ex) {
          LOGGER.severe(() -> "Exception '" + ex.getMessage() + "' occurred on server socket. will terminate.");
          terminate();
        }
      }
    } catch (IOException ex) {
      LOGGER.severe(() -> "Server cannot be started: " + ex.getMessage());
      terminate();
    }
  }

  /**
   * Returns list of connected clients.
   *
   * @return List of connected clients.
   */
  public Client[] getClients() {
    return clients.toArray(Client[]::new);
  }

  public Task[] getTasks() {
    return tasks.toArray(Task[]::new);
  }

  /**
   * Kicks client from server.
   *
   * @param client Client to kick.
   */
  public void kickClient(Client client) {
    client.disconnect();
  }

  /**
   * Removes client from server's list. This calls the client it self when terminating. If you want to remove client from server use kickClient() method insted.
   *
   * @param client Client to remove.
   */
  public void removeClient(Client client) {
    synchronized (clientLock) {
      if (client.getClientState() == ClientState.COMPUTING) {
        failedTask(client, "Node disconnected!");
      }

      LOGGER.fine(() -> "Removing client " + client + ".");
      clients.remove(client);
      listeners.forEach(l -> l.clientsChanged("Client " + client + " disconnected."));
      sendClientsInfo();
    }
  }

  /**
   * Updates clients and informs them weather something changed.
   */
  private void updateClients() {
    //  When computing we don't want to add new nodes. Also, when clients didn't change there is no point updating.
    if (newClients) {
      synchronized (clientLock) {
        LOGGER.fine(() -> "Client update timer.");
        //  Change status of newly connected clients
        clients.stream()
            .filter(Client::isConnected)
            .filter(Client::hasUuid)
            .forEach(c -> {
              c.setClientState(ClientState.READY);
              LOGGER.fine(() -> "Changed clients state to READY on " + c);
            });
        //  Inform all clients about connected clients
        sendClientsInfo();

        listeners.forEach(l -> l.clientsChanged("Updated connected clients with UUID to ready state."));
        newClients = false;
        triggerTaskStart();
      }
    }
  }

  /**
   * This method stops server.
   */
  public void terminate() {
    running = false;
  }

  /**
   * Adds new server listener.
   *
   * @param listener Listener to add.
   */
  public void addListener(ServerListener listener) {
    listeners.add(listener);
  }

  /**
   * Assigns given UUID to a given client.
   *
   * @param uuid   UUID to assign.
   * @param client Client to whom assign UUID.
   * @throws ReservedUuidException When the UUID is reserved.
   * @throws UuidAlreadyInUse      When the UUID is already in use.
   */
  public void assignUuidToClient(String uuid, Client client) throws ReservedUuidException, UuidAlreadyInUse, AlreadyHasUuidException {
    if (client.getUuid() != null) {
      throw new AlreadyHasUuidException("Already has an UUID with value: " + client.getUuid().toString() + "!");
    } else if (uuid.equalsIgnoreCase("server")) {
      throw new ReservedUuidException("'server' is a reserved UUID for server and cannot be assigned to node!");
    } else if (uuid.isEmpty()) {
      throw new ReservedUuidException("UUID cannot be empty!");
    }
    synchronized (clientLock) {
      var count = clients.stream()
          .filter(Client::hasUuid)
          .filter(c -> c.getUuid().toString().equals(uuid))
          .count();

      if (count != 0) {
        throw new UuidAlreadyInUse();
      } else {
        client.setUuid(UUID.fromString(uuid));
        newClients = true;
        listeners.forEach(l -> l.clientsChanged("Assigned UUID to " + client));
      }
    }
  }

  /**
   * Add a new task to the queue and run it when sufficient number of nodes is available.
   *
   * @param name       Name of the task.
   * @param fullName   Full name of the class to be run.
   * @param parameters Parameters for the task.
   * @param nodeCount  Number of nodes on which it should run.
   */
  public void addTask(String name, String fullName, String[] parameters, int nodeCount) {
    // If starting an experiment, only create a experiment director which will spawn all needed tasks
    if(name.equals("ADPP Experiment") || name.equals("COBRA Experiment")) {
      experimentDirector = new MultiAgentExperimentDirector(name, parameters[0], nodeCount);
      return;
    }
    var task = new Task();
    task.setName(name);
    task.setFullName(fullName);
    task.setNodes(nodeCount);
    task.setParameters(parameters);
    task.setState(TaskState.READY);
    tasks.add(task);
    listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " was placed in queue."));
    triggerTaskStart();
  }

  @Override
  public void receivedStatus(Client client, String status) {
    client.setStatus(status);
    listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " send status " + status + "."));
  }

  @Override
  public void receivedStatus(Client client, Message message) {
    switch (message.getType()) {
      case COBRA_NEW_TRAJECTORY:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent new COBRA trajectory."));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      case COBRA_AGENT_INITIALIZED:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent initialized COBRA agent notification."));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      case COBRA_TOKEN:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent token."));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      case COBRA_TOKEN_REQUEST:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent first token request"));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      case ADPP_AGENT_INITIALIZED:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent initialized ADPP agent notification."));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      case ADPP_INNER_MESSAGE:
        listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " sent new ADPP trajectory."));
        multiAgentTaskDirector.receivedAgentStatus(client, message);
        break;
      default:
        LOGGER.info("Server received unknown status update message type");
    }

  }

  /**
   * This method actually starts tasks if there is sufficient number of ready nodes.
   */
  private void triggerTaskStart() {
    synchronized (clientLock) {
      if (clients.size() == 0 || nextTask == tasks.size()) {
        return;
      }


      while (tasks.size() > nextTask) {
        if (clients.size() < tasks.get(nextTask).getNodes()) {
          tasks.get(nextTask).setState(TaskState.FAILED);
          tasks.get(nextTask).setResult("Failed because not enough nodes were connected.");
          listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " failed because not enough nodes are connected."));
          if(experimentDirector != null && experimentDirector.isExperimentRunning()) {
            experimentDirector.registerFinishedTask(tasks.get(nextTask));
          }
          nextTask++;
        } else if (tasks.get(nextTask).getNodes() <= getReadyClientsNumber()) {
          //  get a list of nodes we will be running our task on
          var targets = clients.stream()
              .filter(Client::isReady)
              .sorted((a, b) -> InetAddressUtils.compareAddresses(a.getAddress(), b.getAddress(), a.getPort(), b.getPort()))
              .limit(tasks.get(nextTask).getNodes())
              .collect(Collectors.toList());

          if(tasks.get(nextTask).getName().contains("COBRA") || tasks.get(nextTask).getName().contains("ADPP")) {
            // Parses parameters passed from gui to a format accepted by COBRA or ADPP scenario creator
            String[] args;

            if(tasks.get(nextTask).getName().contains("COBRA")) {
              args = Cobra.prepareCobraArgs(tasks.get(nextTask).getParameters());
            } else {
              args = ADPP.prepareADPPArgs(tasks.get(nextTask).getParameters());
            }
            try{
              if(tasks.get(nextTask).getName().contains("COBRA")) {
                multiAgentTaskDirector = new CobraDirector(args, targets);
              } else {
                multiAgentTaskDirector = new ADPPDirector(args, targets);
              }
            } catch (Exception e) {
              LOGGER.severe("There was an exception creating " + tasks.get(nextTask).getName() + " problem by your input: " + e.getMessage());
              tasks.get(nextTask).setState(TaskState.FAILED);
              tasks.get(nextTask).setResult("Failed due to malformed parameters");
              listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " of a problem while parsing arguments: " + e.getMessage()));
              if(experimentDirector != null && experimentDirector.isExperimentRunning()) {
                experimentDirector.registerFinishedTask(tasks.get(nextTask));
              }
              nextTask++;
              continue;
            }
            // Fail the task if the number of clients assigned to this task is lower than the number of agents required to solve the problem
            if(!multiAgentTaskDirector.enoughClientsRegistered()) {
              tasks.get(nextTask).setState(TaskState.FAILED);
              tasks.get(nextTask).setResult(String.format("Failed because this " + tasks.get(nextTask).getName() + " problem needs %d agents, but only %d clients are registered and ready", multiAgentTaskDirector.getClientsRequired(), clients.size()));
              listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " failed because it needed more ready clients to compute agents."));
              if(experimentDirector != null && experimentDirector.isExperimentRunning()) {
                experimentDirector.registerFinishedTask(tasks.get(nextTask));
              }
              nextTask++;
              continue;
            }
            tasks.get(nextTask).setParameters(AbstractAgentsAlgorithm.prepareArgsForNode(args, multiAgentTaskDirector.getProblemFileContent()));
          }
          //  create a new start task DTO
          var startTask = new StartTask(
              tasks.get(nextTask).getFullName(),
              tasks.get(nextTask).getParameters(),
              targets.stream().map(t -> t.getUuid().toString()).collect(Collectors.toList()).toArray(String[]::new));
          //  add files to storage
          AbstractAlgorithm algorithm;
          try {
            algorithm = (AbstractAlgorithm) Class.forName(tasks.get(nextTask).getFullName()).getDeclaredConstructor().newInstance();
            algorithm.getDistributedFiles().forEach((id, file) -> {
              try {
                Distributor.getDistributor().addDataSource(id, file);
              } catch (NoDataServerIsRunning e) {
                LOGGER.severe(e.getMessage());
              }
            });
          } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
            LOGGER.severe(ex.getMessage());
          }
          //  send message and start each target node
          targets.forEach(t -> {
            t.setTask(tasks.get(nextTask));
            t.setClientState(ClientState.COMPUTING);
            t.send(new Message("server", t.getUuid().toString(), gson.toJson(startTask), MessageType.START));
          });
          //  update task data and notify listeners
          tasks.get(nextTask).setState(TaskState.RUNNING);
          tasks.get(nextTask).setStartTime();
          listeners.forEach(l -> l.tasksChanged("Task " + tasks.get(nextTask).getName() + " started."));
          nextTask++;
        } else {
          break;
        }
      }
    }
  }

  /**
   * Returns number of ready nodes connected to the server.
   *
   * @return Number of ready nodes.
   */
  private long getReadyClientsNumber() {
    return clients.stream().filter(Client::isReady).count();
  }

  /**
   * Stops selected task. Does nothing when there is no task running.
   */
  public void stopTask(Task task) {
    synchronized (clientLock) {
      if (task.getState() == TaskState.RUNNING) {
        clients.stream().filter(c -> c.getTask() == task).forEach(c -> {
          c.send(new Message("server", c.getUuid().toString(), "", MessageType.STOP));
          c.setClientState(ClientState.STOPPING);
        });
        task.setState(TaskState.STOPPED);
        listeners.forEach(l -> l.clientsChanged("Stopping clients working on " + task.getName()));
        listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " stopped."));
        if ((task.getName().equals("COBRA") || task.getName().equals("ADPP")) && multiAgentTaskDirector != null) {
          multiAgentTaskDirector.stopSimulation();
          multiAgentTaskDirector = null;
        }
        if(experimentDirector != null) {
          experimentDirector.registerFinishedTask(task);
        }
      }
    }
  }

  /**
   * Sends information about all connected clients to all connected clients.
   */
  private void sendClientsInfo() {
    var dtos = clients.stream()
        .filter(c -> c.isReady() || c.isComputing())
        .map(Client::toNodeDto)
        .collect(Collectors.toList());
    clients.stream()
        .filter(c -> c.isReady() || c.isComputing())
        .forEach(c -> c.send(new Message(
            "server",
            c.getUuid().toString(),
            gson.toJson(dtos.toArray(new NodeDto[0])),
            MessageType.NODES
        )));
  }

  /**
   * Called when a message should be logged.
   *
   * @param message Mesage to log.
   */
  public void logMessage(String message) {
    listeners.forEach(l -> l.logMessage(message));
  }

  /**
   * Suspend the given client. Client must be in ready state.
   *
   * @param client Client to suspend.
   */
  public void suspendClient(Client client) {
    if (client.isReady()) {
      synchronized (clientLock) {
        client.setClientState(ClientState.SUSPENDED);
        listeners.forEach(l -> l.clientsChanged("Suspended node " + client.getUuid().toString()));
      }
    }
  }

  /**
   * Unsuspend client.
   *
   * @param client Client to unsuspend.
   */
  public void unsuspendClient(Client client) {
    if (client.isSuspended()) {
      synchronized (clientLock) {
        client.setClientState(ClientState.READY);
        listeners.forEach(l -> l.clientsChanged("Unsuspended node " + client.getUuid().toString()));
        triggerTaskStart();
      }
    }
  }

  @Override
  public void finishedTask(Client client, String partialResult) {
    synchronized (clientLock) {
      if (client.isStopping()) {
        stoppedTask(client);
        return;
      }
      if (!client.isComputing()) {
        return;
      }

      var task = client.getTask();

      task.addPartialResult(client.getAddress().getHostAddress() + client.getPort(), partialResult);
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");

      listeners.forEach(l -> l.clientsChanged("Node " + client.getUuid().toString() + " finished task " + task.getName() + "with partial result " + partialResult + "."));

      var count = clients.stream().filter(c -> c.getTask() == task).count();

      if (count == 0) {
        AbstractAlgorithm algorithm;
        try {
          algorithm = (AbstractAlgorithm) Class.forName(task.getFullName()).getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
          LOGGER.severe(ex.getMessage());
          return;
        }
        String result = algorithm.composeResult(task.getPartialResults().toArray(String[]::new));
        LOGGER.info(() -> "Finished " + task.getName() + " with result " + result + ".");
        task.setResult(result);
        task.setState(TaskState.FINISHED);
        task.setEndTime();
        listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " finished."));
        if(experimentDirector != null && experimentDirector.isExperimentRunning()) {
          experimentDirector.registerFinishedTask(task);
        }
      }

      triggerTaskStart();
    }
  }

  @Override
  public void failedTask(Client client, String cause) {
    synchronized (clientLock) {
      if (!client.isComputing()) {
        return;
      }
      var task = client.getTask();
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");
      clients.stream()
          .filter(c -> c.getTask() == task)
          .forEach(c -> {
            c.send(new Message("server", c.getUuid().toString(), "", MessageType.STOP));
            c.setClientState(ClientState.STOPPING);
          });
      task.setEndTime();
      task.setState(TaskState.FAILED);
      task.setResult("Failed due to: " + cause);

      LOGGER.warning(() -> "Task " + task.getName() + " failed due to " + cause + ".");

      listeners.forEach(l -> l.tasksChanged("Task " + task.getName() + " failed."));
      listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " failed the task " + task.getName() + " due to " + cause));

      if(experimentDirector != null && experimentDirector.isExperimentRunning()) {
        experimentDirector.registerFinishedTask(task);
      }

      triggerTaskStart();
    }
  }

  @Override
  public void stoppedTask(Client client) {
    synchronized (clientLock) {
      if (!client.isStopping()) {
        return;
      }
      client.setTask(null);
      client.setClientState(ClientState.READY);
      client.setStatus("");

      listeners.forEach(l -> l.clientsChanged("Client " + client.getUuid().toString() + " stopped."));

      triggerTaskStart();
    }
  }

  @Override
  public void receivedError(Client client, String description) {
    listeners.forEach(l -> l.receivedError(client, description));
  }
}