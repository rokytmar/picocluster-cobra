package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for FINISHED messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class FinishedHandler extends AbstractHandler {

  public FinishedHandler() {
    handledMessageType = MessageType.FINISHED;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().finishedTask(client, message.getContent());
  }
}
