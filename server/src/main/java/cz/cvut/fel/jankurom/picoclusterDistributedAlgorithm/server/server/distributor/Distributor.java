package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.distributor;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.NoDataServerIsRunning;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Class for distributing shared data to the node.
 */
public final class Distributor {
  private static final Object LOCK = new Object();
  private static Distributor distributor;
  private final HashMap<String, File> data;
  private DistributorServer distributorServer;

  private Distributor() {
    data = new HashMap<>();
  }

  /**
   * Gets the only instance of this class.
   *
   * @return The only instance of this class.
   */
  public static Distributor getDistributor() {
    if (distributor == null) {
      synchronized (LOCK) {
        if (distributor == null) {
          distributor = new Distributor();
        }
      }
    }
    return distributor;
  }

  /**
   * Start a new distributor service. Does nothing if there is already one running.
   *
   * @throws IOException When something goes wrong.
   */
  public void startDistributorServer() throws IOException {
    if (distributorServer != null) {
      return;
    }
    distributorServer = new DistributorServer();
    distributorServer.start();
  }

  /**
   * Used to stop distributor server. Does nothing if it is not running.
   */
  public void stopDistributorServer() {
    if (distributorServer == null) {
      return;
    }
    distributorServer.terminate();
    distributorServer = null;
  }

  /**
   * Add new data source to the server.
   *
   * @param identification Identification of the data.
   * @param source         File with the data.
   * @throws NoDataServerIsRunning When there is no server running.
   */
  public void addDataSource(String identification, File source) throws NoDataServerIsRunning {
    if (distributorServer == null) {
      throw new NoDataServerIsRunning();
    }
    data.put(identification, source);
  }

  /**
   * Remove data from the server with given key. DOes nothing if the key is not present.
   *
   * @param identification Key to delete.
   * @throws NoDataServerIsRunning When there is no distributor server running.
   */
  public void removeDataSource(String identification) throws NoDataServerIsRunning {
    if (distributorServer == null) {
      throw new NoDataServerIsRunning();
    }
    data.remove(identification);
  }

  /**
   * Checks weather there is data with given identification.
   *
   * @param identification Identification to look for.
   * @return True if the ID is present, false otherwise.
   * @throws NoDataServerIsRunning When there is no distributor server running.
   */
  public boolean hasDataResource(String identification) throws NoDataServerIsRunning {
    if (distributorServer == null) {
      throw new NoDataServerIsRunning();
    }
    return data.containsKey(identification);
  }

  /**
   * Gets value for a given id.
   *
   * @param identification Identification of the data.
   * @return Pointer to the file.
   * @throws NoDataServerIsRunning When there is no distributor server running.
   */
  public File getDataSource(String identification) throws NoDataServerIsRunning {
    if (distributorServer == null) {
      throw new NoDataServerIsRunning();
    }
    return data.get(identification);
  }
}