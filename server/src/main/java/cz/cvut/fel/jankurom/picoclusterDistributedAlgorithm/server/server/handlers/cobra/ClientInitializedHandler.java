package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.AbstractHandler;

/**
 * Handler for COBRA_AGENT_INITIALIZED messages.
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class ClientInitializedHandler extends AbstractHandler {
  public ClientInitializedHandler() {
    this.handledMessageType = MessageType.COBRA_AGENT_INITIALIZED;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().receivedStatus(client, message);
  }
}
