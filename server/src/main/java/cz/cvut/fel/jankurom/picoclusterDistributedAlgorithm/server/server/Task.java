package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Representation of a task result.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Task {
  private final HashMap<String, String> partialResults = new HashMap<>();
  private String name;
  private int nodes;
  private String result;
  private TaskState state;
  private String fullName;
  private Instant startTime;
  private Instant endTime;
  private String[] parameters;

  public String[] getParameters() {
    return parameters;
  }

  public void setParameters(String[] parameters) {
    this.parameters = parameters;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStartTime() {
    startTime = Instant.now();
  }

  public void setEndTime() {
    endTime = Instant.now();
  }

  public LocalTime getEnlapsedTime() {
    if (startTime == null) {
      return LocalTime.of(0, 0, 0);
    } else if (endTime == null) {
      return LocalTime.ofNanoOfDay(Duration.between(startTime, Instant.now()).toNanos());
    }
    return LocalTime.ofNanoOfDay(Duration.between(startTime, endTime).toNanos());
  }

  public int getNodes() {
    return nodes;
  }

  public void setNodes(int nodes) {
    this.nodes = nodes;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public TaskState getState() {
    return state;
  }

  public void setState(TaskState state) {
    this.state = state;
  }

  public void addPartialResult(String address, String partialResult) {
    partialResults.put(address, partialResult);
  }

  public void clearPartialResults() {
    partialResults.clear();
  }

  public List<String> getPartialResults() {
    List<String> list = new ArrayList<>();
    list.addAll(partialResults.values());
    return list;
  }

  @Override
  public String toString() {
    return name + " [" + state + "]";
  }
}
