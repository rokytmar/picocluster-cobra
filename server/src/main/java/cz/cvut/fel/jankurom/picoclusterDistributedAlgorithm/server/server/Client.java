package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.NodeDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.NoHandlerForMessageTypeException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.listeners.ClientListener;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.HandlersAccessor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Representation of a client that is connected to the server.
 *
 * @author Roman Janků
 */
public class Client extends Thread {
  /**
   * Maximum length of a message that gets printed by logger
   */
  private static final int MAX_MSG_LOGGER_LENGTH = 1000;

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");
  /**
   * Socket to the client program.
   */
  private final Socket socket;
  /**
   * Gson for encoding and decoding JSON.
   */
  private final Gson gson;
  /**
   * List of client listeners registered on this client.
   */
  private final List<ClientListener> listeners;
  /**
   * The current state of this client.
   */
  private ClientState state;
  /**
   * UUID of the connected client.
   */
  private UUID uuid;
  /**
   * UDP port this client is operating on
   */
  private int udpPort;
  /**
   * Input buffered reader for data from node.
   */
  private BufferedReader in;
  /**
   * Output buffered writer for data to node.
   */
  private BufferedWriter out;
  /**
   * Variable determining weather the client is running.
   */
  private boolean run;
  /**
   * Task on which the client is working.
   */
  private Task task;
  /**
   * Status of the client.
   */
  private String status;

  /**
   * Creates a new client with a given socket.
   *
   * @param socket Socket on which client is connected.
   */
  public Client(Socket socket) throws IOException {
    this.socket = socket;
    this.state = ClientState.CONNECTED;
    this.gson = new Gson();
    this.uuid = null;
    this.udpPort = 0;
    this.listeners = new ArrayList<>();
    run = true;

    this.socket.setSoTimeout(2500);
  }

  /**
   * Returns the client's status.
   *
   * @return Client's status
   */
  public String getStatus() {
    return status;
  }

  /**
   * Sets client's status.
   *
   * @param status Status to set.
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * Returns current task or null if client has no current task.
   *
   * @return Current task or null.
   */
  public Task getTask() {
    return task;
  }

  /**
   * Sets client's current task.
   *
   * @param task New task.
   */
  public void setTask(Task task) {
    this.task = task;
  }

  /**
   * Tells weather client is in connected state.
   *
   * @return True if client is in connected state, false otherwise.
   */
  public boolean isConnected() {
    return state == ClientState.CONNECTED;
  }

  /**
   * Tells weather client is in computing state.
   *
   * @return True if client is in computing state, false otherwise.
   */
  public boolean isComputing() {
    return state == ClientState.COMPUTING;
  }

  /**
   * Tells weather client is in ready state.
   *
   * @return True if client is in ready state, false otherwise.
   */
  public boolean isReady() {
    return state == ClientState.READY;
  }

  /**
   * Tells weather client is in disconnected state.
   *
   * @return True if client is in disconnected state, false otherwise.
   */
  public boolean isDisconnected() {
    return state == ClientState.DISCONNECTED;
  }

  /**
   * Tells weather the client is stopping current task.
   *
   * @return True of client is stopping, false otherwise.
   */
  public boolean isStopping() {
    return state == ClientState.STOPPING;
  }

  /**
   * Tells weather the client is in SUSPENDED state.
   *
   * @return True if client is in suspended state, false otherwise.
   */
  public boolean isSuspended() {
    return state == ClientState.SUSPENDED;
  }

  /**
   * Returns the current state of the client.
   *
   * @return Current state of the client.
   */
  public ClientState getClientState() {
    return state;
  }

  /**
   * Sets the current state of the client.
   *
   * @param state New state of the client.
   */
  public void setClientState(ClientState state) {
    this.state = state;
  }

  /**
   * Sends given message to the client.
   *
   * @param message Message to send.
   */
  public void send(Message message) {
    try {
      out.write(gson.toJson(message) + "\n");
      out.flush();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public void run() {
    try {
      this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      this.out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    } catch (IOException ex) {
      ex.printStackTrace();
      return;
    }

    while (run) {
      Message message = null;
      try {
        var line = in.readLine();
        LOGGER.info(() -> "Received: " + (line.length() > MAX_MSG_LOGGER_LENGTH ?
            "Message too long. (" + line.length() + "B) First " + MAX_MSG_LOGGER_LENGTH + " chars: " + line.substring(0, MAX_MSG_LOGGER_LENGTH) : line));
        message = gson.fromJson(line, Message.class);

        //  null message means closed socket, we will terminate
        if (message == null) {
          run = false;
          LOGGER.warning(() -> "Received null messasge on " + this + ". Will terminate.");
          break;
        }

        HandlersAccessor.getHandlerForMessageType(message.getType()).handleMessage(message, this);
      } catch (SocketTimeoutException ex) {
        //  This is ignored and only implemented to gracefully shutdown
        LOGGER.config(() -> "Timeout on '" + uuid + "'.");
      } catch (IOException ex) {
        //  Some IO exception happened and we cannot fix it so we will terminate.
        LOGGER.severe(() -> "Error occurred when communicating with client '" + uuid.toString() + "'. Will terminate. Cause: " + ex.getMessage());
        run = false;
      } catch (NoHandlerForMessageTypeException ex) {
        //  We have received an unknown message type
        LOGGER.warning(() -> "No handler on '" + uuid.toString() + "'. Cause: " + ex.getMessage());
        send(new Message("server", message.getFrom(), ex.getMessage(), MessageType.ERROR));
      }
    }
    Server.getInstance().removeClient(this);
  }

  @Override
  public String toString() {
    return "[" + socket.getInetAddress() + ":" + socket.getPort() + "] " + (uuid == null ? "<NULL>" : uuid.toString());
  }

  /**
   * Adds new client listener.
   *
   * @param listener Listener to add.
   */
  public void addClientListener(ClientListener listener) {
    listeners.add(listener);
  }

  /**
   * Removes listener. If the listener was never added, nothing happens.
   *
   * @param listener Listener to remove.
   */
  public void removeClientListener(ClientListener listener) {
    listeners.remove(listener);
  }

  /**
   * Returns client's UUID.
   *
   * @return client's UUID.
   */
  public UUID getUuid() {
    return uuid;
  }

  /**
   * Sets client's UUID.
   *
   * @param uuid New client's UUID.
   */
  public void setUuid(UUID uuid) {
    this.uuid = uuid;
  }

  /**
   * Tells weather client has UUID.
   *
   * @return TRUE if client has UUID, FALSE otherwise.
   */
  public boolean hasUuid() {
    return uuid != null;
  }

  /**
   * Returns client's address.
   *
   * @return Client's address.
   */
  public InetAddress getAddress() {
    return socket.getInetAddress();
  }

  /**
   * Returns client's port.
   *
   * @return Client's port.
   */
  public int getPort() {
    return socket.getPort();
  }

  /**
   * Returns client's UDP port.
   *
   * @return Client's UDP port.
   */
  public int getUDPPort() {
    return udpPort;
  }

  /**
   * Sets a udp port this client uses
   * @param clientUdpPort
   */
  public void setUdpPort(Integer clientUdpPort) {
    this.udpPort = clientUdpPort;
  }

  /**
   * Closes connection to the node.
   */
  public void disconnect() {
    run = false;
    try {
      join();
    } catch (InterruptedException ex) {
      LOGGER.severe(() -> "Could not properly closed connection with " + this + ". Cause: " + ex.getMessage());
    }
  }

  /**
   * Converts Client into NodeDto for communication.
   *
   * @return NodeDto created from the client.
   */
  public NodeDto toNodeDto() {
    var dto = new NodeDto();
    dto.setAddress(getAddress());
    dto.setPort(getPort());
    dto.setUuid(getUuid());
    dto.setUdpPort(getUDPPort());
    return dto;
  }
}