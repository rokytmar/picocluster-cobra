package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.multiAgentExperiment;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.adpp.ADPP;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.cobra.Cobra;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Task;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.TaskState;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * An experiment director taking care of the server-side of the experiment
 */
public class MultiAgentExperimentDirector {
    private static final Logger LOGGER = Logger.getLogger("picocluster-server");

    private enum AlgorithmType {
        COBRA,
        ADPP;
    }

    private static final String DATA_IN_FILE_NAME = "data.in";

    private static final String DATA_OUT_FILE_NAME = "data.out";

    private static final String COBRA_INSTANCE_PATH = "project/cobra-icaps2015/experiment/instances/";
    private static final String ADPP_INSTANCE_PATH = "project/adpp-journal/instances/";
    /**
     * All supportet instance set names for COBRA
     */
    private static final String[] COBRA_INSTANCES = {"empty-hall-r25", "warehouse-r25", "ubremen-r27"};
    /**
     * All supportet instance set names for ADPP
     */
    private static final String[] ADPP_INSTANCES = {"empty-hall-r22-docks", "warehouse-r25-docks", "ubremen-r27-docks"};

    /**
     * List of all tasks created for the experiment
     */
    private List<Task> startingTasks;
    /**
     * List of all finished tasks of the experiment
     */
    private List<Task> finishedTasks;

    /**
     * Algorithm type used for this experiment
     */
    private AlgorithmType algorithm;

    /**
     * String array containing working instances of current algorithm
     */
    private String[] instances;

    /**
     * Path to instances for current algorithm
     */
    private String instancePath;

    /**
     * Number of nodes participating in the experiment
     */
    private int nodeCount;

    /**
     * Flag enabling the visualization
     */
    private boolean showVis;

    /**
     * System timestamp of the experiment's start in ms
     */
    private long timeStarted;

    /**
     * System timestamp of the experiment's end in ms
     */
    private long timeFinished;

    private boolean experimentRunning;

    /**
     * Summary prefixes for each instances load from the input data and used as a prefix for output data
     */
    private List<String> summaryPrefixes;


    public MultiAgentExperimentDirector(String experimentName, String showVisParam, int nodeCount) {
        this.showVis = showVisParam.equals("true") || showVisParam.equals("TRUE");
        this.nodeCount = nodeCount;
        if(experimentName.equals("COBRA Experiment")) {
            algorithm = AlgorithmType.COBRA;
            instances = COBRA_INSTANCES;
            instancePath = COBRA_INSTANCE_PATH;
        } else if (experimentName.equals("ADPP Experiment")) {
            algorithm = AlgorithmType.ADPP;
            instances = ADPP_INSTANCES;
            instancePath = ADPP_INSTANCE_PATH;
        } else {
            LOGGER.severe("Experiment director was started but " + experimentName + " is not a recognized experiment");
            return;
        }

        summaryPrefixes = new ArrayList<>();
        startingTasks = new ArrayList<>();
        finishedTasks = new ArrayList<>();

        parseInputData();
        LOGGER.info("Starting experiment director for " + experimentName);
        timeStarted = System.currentTimeMillis();
        experimentRunning = true;

        // Add all experiment instances as task
        if(!startingTasks.isEmpty()) {
            startingTasks.forEach(task -> {
                Server.getInstance().addTask(task.getName(), task.getFullName(), task.getParameters(), task.getNodes());
            });
        }
    }

    /**
     * Registers a finished task of the experiment. All tasks should be passed here, no matter their result
     * @param task Task that has finished
     */
    public void registerFinishedTask(Task task) {
        finishedTasks.add(task);

        //Experimet ginished
        if(finishedTasks.size() == startingTasks.size()) {
            timeFinished = System.currentTimeMillis();
            long successfulTasks = finishedTasks.stream().filter(tsk -> tsk.getState() == TaskState.FINISHED).count();
            long stoppedTasks = finishedTasks.stream().filter(tsk -> tsk.getState() == TaskState.STOPPED).count();
            long failedTasks = finishedTasks.stream().filter(tsk -> tsk.getState() == TaskState.FAILED).count();
            String experimentFinishedInfo = String.format("Finished %s experiment in %dms. Successful tasks: %d; Failed tasks: %d; Stopped tasks: %d", algorithm, timeFinished - timeStarted, successfulTasks, failedTasks, stoppedTasks);
            LOGGER.info(experimentFinishedInfo);
            Server.getInstance().logMessage(experimentFinishedInfo);
        }
        saveResults();
    }

    /**
     * Parses the input data for all instances used in the experiment
     */
    private void parseInputData() {
        List<String> inputs = new ArrayList<>();
        // Load data from input file in each instance directory
        for (String instance : instances) {
            try {
                File dataFile = new File(String.format("%s%s/%s", instancePath, instance, DATA_IN_FILE_NAME));
                Scanner reader = new Scanner(dataFile);
                while(reader.hasNextLine()) {
                    inputs.add(reader.nextLine());
                }
                reader.close();
            } catch (Exception e) {
               LOGGER.severe("An exception has occurred when loading experiment inputs");
               e.printStackTrace();
               return;
            }
        }

        for (int i = 0; i < inputs.size(); i++) {
            String input = inputs.get(i);
            summaryPrefixes.add(input.split(" ")[input.split(" ").length - 1]); // Add last element of split input to summary prefixes
            startingTasks.add(parseTask(input, i, inputs.size()));
        }
    }

    /**
     * Parses input data into algorithm Task for the experiment
     * @param args Input arguments for current instance
     * @param num serial number of the instance
     * @param totalCount total number of instances in the experiment
     * @return Task instance created from the input data
     */
    private Task parseTask(String args, int num, int totalCount) {
        String[] parsedArgs = {};
        String algFullName = "";
        switch (algorithm) {
            case ADPP:
                parsedArgs = ADPP.parseArgumentsForPicocluster(args, showVis);
                algFullName = ADPP.class.getName();
            break;
            case COBRA:
                parsedArgs = Cobra.parseArgumentsForPicocluster(args, showVis);
                algFullName = Cobra.class.getName();
                break;
        }

        Task task = new Task();
        task.setName(String.format("%s %d/%d %s", algorithm.toString(), num + 1, totalCount, parsedArgs[0]));
        task.setFullName(algFullName);
        task.setNodes(nodeCount);
        task.setParameters(parsedArgs);
        return task;
    }

    /**
     * Saves results of all instances into corresponding output files
     */
    private void saveResults() {
        for (String instance : instances) {
            File outFile = new File(String.format("%s%s/%s", instancePath, instance, DATA_OUT_FILE_NAME));
            try {
                Writer writer = new FileWriter(outFile);
                for (int i = 0; i < finishedTasks.size(); i++) {
                    if(finishedTasks.get(i).getState() == TaskState.FINISHED && summaryPrefixes.get(i).contains(instance)) {
                        writer.write(summaryPrefixes.get(i) + finishedTasks.get(i).getResult() + "\n");
                    }
                }
                writer.close();
                LOGGER.info("Wrote " + algorithm + " experiment for instanceset " + instance + " to " + outFile.getPath());
            } catch (IOException e) {
                LOGGER.warning("An exception occurred while writing " + algorithm + " experiment results to file");
                e.printStackTrace();
            }
        }
    }

    public boolean isExperimentRunning() {
        return experimentRunning;
    }
}
