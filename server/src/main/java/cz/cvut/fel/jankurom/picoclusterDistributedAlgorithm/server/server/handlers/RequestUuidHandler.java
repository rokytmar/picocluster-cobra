package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.AlreadyHasUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.ReservedUuidException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.UuidAlreadyInUse;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for handling UUID_REQUEST messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class RequestUuidHandler extends AbstractHandler {

  /**
   * Creates new handler.
   */
  public RequestUuidHandler() {
    handledMessageType = MessageType.UUID_REQUEST;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    if (client.getUuid() == null) {
      try {
        String uuid = message.getContent();
        Integer clientUdpPort = null;

        int dividerPos = message.getContent().indexOf('/');
        if(dividerPos != -1) {
          uuid = message.getContent().substring(0, dividerPos);
          clientUdpPort = Integer.parseInt(message.getContent().substring(dividerPos + 1));
        }

        Server.getInstance().assignUuidToClient(uuid, client);

        if(clientUdpPort != null) {
          client.setUdpPort(clientUdpPort);
        }

        client.send(new Message("server", client.getUuid().toString(), client.getUuid().toString(), MessageType.UUID_CONFIRM));
      } catch (ReservedUuidException | UuidAlreadyInUse | AlreadyHasUuidException ex) {
        //  TODO send UUID_REJECT
      }
    }
  }
}
