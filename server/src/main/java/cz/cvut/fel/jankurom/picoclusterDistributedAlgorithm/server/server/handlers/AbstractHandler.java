package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;

/**
 * Abstract handler defining methods for message handlers. Any handler can handle only type of message.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public abstract class AbstractHandler {

  /**
   * Message handled by the handler. It is null by default and descendant must override this.
   */
  protected MessageType handledMessageType = null;

  /**
   * Tells weather the given message type is handled by the handler.
   *
   * @param type Message type to test.
   * @return True if handler handles this type of message, false if it does not.
   */
  public boolean isHandlingMessageType(MessageType type) {
    return type == handledMessageType;
  }

  /**
   * Returns the MessageType handled by this handler.
   *
   * @return Messagetype handled by this handler.
   */
  public MessageType getHandledMessageType() {
    return handledMessageType;
  }

  /**
   * Gives handler a message to handle.
   *
   * @param message Message to handle.
   * @param client  Client form which it was received.
   */
  public abstract void handleMessage(Message message, Client client);
}
