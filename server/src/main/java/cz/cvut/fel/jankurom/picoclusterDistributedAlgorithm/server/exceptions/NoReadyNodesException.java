package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Thrown when user wants to start task but there is no ready node.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class NoReadyNodesException extends Exception {

  public NoReadyNodesException() {
    super();
  }

  public NoReadyNodesException(String message) {
    super(message);
  }
}
