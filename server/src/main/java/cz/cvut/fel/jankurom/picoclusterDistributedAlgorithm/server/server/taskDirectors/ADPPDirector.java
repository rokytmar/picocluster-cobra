package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.agents.admap.agent.ADPPAgent;
import cz.agents.admap.agent.Agent;
import cz.agents.admap.creator.ScenarioCreator;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.agents.utils.InnerADPPMessageDtoDeserializer;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.InnerADPPMessageDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.StartRemoteADPPAgent;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content.ContentDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.adpp.content.InformNewTrajectoryDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils.InetAddressUtils;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import tt.jointeuclid2ni.probleminstance.EarliestArrivalProblem;
import tt.jointeuclid2ni.probleminstance.VisUtil;
import tt.jointeuclid2ni.solver.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Server side director of the ADPP algorithm
 */
public class ADPPDirector implements MultiAgentTaskDirector {
    protected static final Logger LOGGER = Logger.getLogger("picocluster-server");
    /**
     * Gson with registered additional adapter for correct InnerADPPMessageDto deserialization
     */
    protected static Gson gson = new GsonBuilder()
        .registerTypeAdapter(InnerADPPMessageDto.class, new InnerADPPMessageDtoDeserializer())
        .create();

    private static final int TICK_INTERVAL_NS = 100;

    protected List<Client> clients;
    private Parameters params;

    private EarliestArrivalProblem problem;

    private File problemFile;

    /**
     * Set preserving all agents that have already sent the ready message
     */
    protected Set<String> initializedAgents;
    /**
     * A map mapping agents ID, to a Pair consisting of a Client instance that runs this agent, and server's copy of the Agent
     */
    protected Map<Integer, Pair<Client, Agent>> agentClients;

    public ADPPDirector(String[] args, List<Client> clients) {
        this.params = ScenarioCreator.createFromArgs(args);
        this.problem = ScenarioCreator.getProblem();
        this.problemFile = ScenarioCreator.getProblemFile();

        this.clients = clients;
        agentClients = new HashMap<>();
        initializedAgents = new HashSet<>();

        stopSimulation();

        // Sort clients so that they receive IDs based on their ip and port
        clients.sort((a, b) -> InetAddressUtils.compareAddresses(a.getAddress(), b.getAddress(), a.getPort(), b.getPort()));

        for (int i = 0; i < clients.size(); i++) {
            Client c = clients.get(i);
            Agent agent = null;
            if(i < problem.nAgents()) {
                agent = new ADPPAgent(String.valueOf(i),
                    problem.getStart(i),
                    problem.getTarget(i),
                    problem.getEnvironment(),
                    problem.getBodyRadius(i),
                    params.maxTime, params.waitMoveDuration, problem.getObstacles());
            }
            agentClients.put(i, new Pair<>(c, agent));
        }
    }

    @Override
    public void startSimulation() {
        Map<Integer, UUID> idToUUID = new HashMap<>();
        for (int i = 0; i < clients.size(); i++) {
            idToUUID.put(i, clients.get(i).getUuid());
        }
        agentClients.forEach((i, acPair) -> {
            Client c = acPair.getFirst();
            c.send(new Message("server", c.getUuid().toString(),
                gson.toJson(new StartRemoteADPPAgent(i, TICK_INTERVAL_NS , idToUUID)), MessageType.ADPP_START_AGENT));
        });

        if(params.showVis) {
            VisUtil.initVisualization(problem, "Trajectory Tools (ADPP)", params.bgImageFile, params.timeStep/2);
            VisUtil.visualizeEarliestArrivalProblem(problem);

            ScenarioCreator.visualizeAgents(problem, getAgents());
            ScenarioCreator.visualizeConflicts(getAgents());
        }

    }

    @Override
    public boolean enoughClientsRegistered() {
        return clients.size() >= problem.nAgents();
    }

    @Override
    public int getClientsRequired() {
        return problem.nAgents();
    }

    @Override
    public void receivedAgentStatus(Client client, Message message) {
        switch (message.getType()) {
            case ADPP_AGENT_INITIALIZED:
                addInitializedAgent(client);
                break;
            case ADPP_INNER_MESSAGE:
                handleInnerMessage(client, message);
                break;
        }

    }

    private void handleInnerMessage(Client client, Message message) {
        InnerADPPMessageDto innerDto = gson.fromJson(message.getContent(), InnerADPPMessageDto.class);
        switch (innerDto.getMessageType()) {
            case INFORM_NEW_TRAJECTORY:
                InformNewTrajectoryDto newTrajectoryDto = (InformNewTrajectoryDto) innerDto.getContentDto();
                parseNewTrajectory(newTrajectoryDto);
                break;
            default:
                LOGGER.warning("Server has received a " + innerDto.getMessageType() + " but does not know what to do with it");
        }
    }

    private void parseNewTrajectory(InformNewTrajectoryDto newTrajectory) {
        Agent agent = agentClients.get(Integer.valueOf(newTrajectory.getAgentName())).getSecond();
        agent.setCurrentTrajectory(newTrajectory.getMovingCircleDto().getTrajectory());
    }

    @Override
    public void addInitializedAgent(Client client) {
        initializedAgents.add(client.getUuid().toString());
        if(initializedAgents.size() == agentClients.size()) {
            startSimulation();
        }
    }

    /**
     * Returns an agent bound to a client
     * @param client Client that runs an agent
     * @return A server's local copy of Agent that is controlled by the client
     */
    public Agent getAgentByClient(Client client) {
        for (Pair<Client, Agent> acPair : agentClients.values()) {
            if(client.equals(acPair.getFirst()))
                return acPair.getSecond();
        }
        return null;
    }

    public List<Agent> getAgents() {
        List<Agent> agents = new ArrayList<>();
        for (Pair<Client, Agent> acPair : agentClients.values()) {
            if(acPair.getSecond() == null) continue;
            agents.add(acPair.getSecond());
        }
        return agents;
    }

    @Override
    public void stopSimulation() {

    }

    public String getProblemFileContent() {
        try {
            return FileUtils.readFileToString(problemFile);
        } catch (IOException e) {
            LOGGER.severe("Could not load content of problem file to pass to nodes!");
            return null;
        }
    }
}
