package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server;

/**
 * Representation of a state of a task.
 */
public enum TaskState {
  /**
   * No task.
   */
  NONE("NONE"),
  /**
   * Task is ready to be computed.
   */
  READY("READY"),
  /**
   * Task is running.
   */
  RUNNING("RUNNING"),
  /**
   * Task successfully finished.
   */
  FINISHED("FINISHED"),
  /**
   * Task failed.
   */
  FAILED("FAILED"),
  /**
   * Task was stopped by user.
   */
  STOPPED("STOPPED");

  private final String name;

  TaskState(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
