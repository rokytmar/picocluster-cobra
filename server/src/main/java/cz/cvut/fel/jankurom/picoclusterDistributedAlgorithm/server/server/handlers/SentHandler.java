package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;

/**
 * Handler for SENT messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class SentHandler extends AbstractHandler {

  public SentHandler() {
    handledMessageType = MessageType.SENT;
  }

  @Override
  public void handleMessage(Message message, Client client) {
    Server.getInstance().logMessage(client.getUuid().toString() + " sent message to " + message.getContent() + ".");
  }
}