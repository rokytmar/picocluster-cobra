package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exporters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Task;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;

/**
 * Export for JSON files.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class JsonExporter extends AbstractExporter {

  private final Gson gson;

  /**
   * Creates a new JsonExporter.
   */
  public JsonExporter() {
    super("json");
    //  create a new JSON with custom serializer for java.time.Instant
    gson = new GsonBuilder()
        .registerTypeAdapter(
            Instant.class,
            (JsonSerializer<Instant>) (instant, type, jsonSerializationContext) -> new JsonPrimitive(instant.toString())
        ).setPrettyPrinting().create();
  }

  @Override
  public void export(Task[] tasks, File file) throws IOException {
    var json = gson.toJson(tasks);

    if (!file.exists()) {
      file.createNewFile();
    }

    var fw = new FileWriter(file);
    fw.write(json);
    fw.flush();
    fw.close();
  }
}
