package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors;

import com.google.gson.Gson;
import cz.agents.map4rt.CommonTime;
import cz.agents.map4rt.ScenarioCreator;
import cz.agents.map4rt.agent.Agent;
import cz.agents.map4rt.agent.COBRAAgent;
import cz.agents.map4rt.agent.CurrentTasks;
import cz.agents.map4rt.agent.Token;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.NewTrajectoryNotification;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.StartRemoteCobraAgent;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.TokenDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.agents.cobra.TokenRequest;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.utils.InetAddressUtils;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.util.Pair;
import tt.euclid2i.trajectory.BasicSegmentedTrajectory;
import tt.euclidtime3i.discretization.Straight;
import tt.euclidtime3i.region.MovingCircle;
import tt.jointeuclid2ni.probleminstance.RelocationTaskCoordinationProblem;
import tt.jointeuclid2ni.solver.Parameters;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

/**
 * A sub-server taking care of all COBRA-related agenda
 *
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public  class CobraDirector implements MultiAgentTaskDirector {
  protected static final Logger LOGGER = Logger.getLogger("picocluster-server");
  protected static Gson gson = new Gson();
  private static final int tickLength = 100;
  private static final int FIRST_TASK_WINDOW = 30000;

  private Parameters params;
  private RelocationTaskCoordinationProblem problem;

  private File problemFile;

  protected List<Client> clients;
  /**
   * A map mapping agents ID, to a Pair consisting of a Client instance that runs this agent, and server's copy of the Agent
   */
  protected Map<Integer, Pair<Client, Agent>> agentClients;

  /**
   * Set preserving all agents that have already sent the ready message
   */
  protected Set<String> initializedAgents;

  /**
   * Specifies whether the token is already passing
   * The server holds the first token to fill it with the docked trajectories, and sends the token to the network after first request.
   */
  private boolean tokenPassing;

  public CobraDirector(String[] args, List<Client> clients) {
    this.params = ScenarioCreator.createFromArgs(args);
    this.problem = ScenarioCreator.getProblem();
    this.problemFile = ScenarioCreator.getProblemFile();
    this.clients = clients;

    agentClients = new HashMap<>();
    initializedAgents = new HashSet<>();

    // Stops simulation if there was any
    stopSimulation();

    // Sort clients so that they receive IDs based on their ip and port
    clients.sort((a, b) -> InetAddressUtils.compareAddresses(a.getAddress(), b.getAddress(), a.getPort(), b.getPort()));


    for (int i = 0; i < clients.size(); i++) {
      Client c = clients.get(i);
      Agent agent = null;
      if(i < problem.nAgents()) {
        agent = new COBRAAgent(String.valueOf(i),
            problem.getStart(i),
            params.nTasks,
            problem.getEnvironment(),
            problem.getPlanningGraph(),
            problem.getBodyRadius(i),
            problem.getMaxSpeed(i),
            params.maxTime, params.timeStep, params.random);
        Token.register(agent.getName(), new MovingCircle(agent.getCurrentTrajectory(), agent.getAgentBodyRadius(), params.timeStep / 2));
      }
      agentClients.put(i, new Pair<>(c, agent));
    }
  }

  /**
   * Start the simulation
   */
  public void startSimulation() {
    long clockStartedAt = CommonTime.getStartedAt(); // Timestamp of server's simulation clockm used to sync the nodes
    long initializedAt = CommonTime.currentTimeMs(); // Simulation time when the task begun
    // Send StartRemoteAgent message to each client
    agentClients.forEach((i, acPair) -> {
      Client c = acPair.getFirst();
      c.send(new Message("server", c.getUuid().toString(), gson.toJson(new StartRemoteCobraAgent(i, tickLength, clockStartedAt, initializedAt, params.random.nextInt(FIRST_TASK_WINDOW))), MessageType.COBRA_START_AGENT));
    });

    // Start the visualization in separate thread, if the visualization is set
    if(params.showVis) {
      Thread visThread = new Thread() {
        @Override
        public void run() {
          ScenarioCreator.startRemoteVisualization(getAgents(), params);
        }
      };
      visThread.start();
      LOGGER.info("Started COBRA visualization");
    }
  }

  public boolean enoughClientsRegistered() {
    return clients.size() >= problem.nAgents();
  }

  public int getClientsRequired() {
    return problem.nAgents();
  }

  /**
   * Handler taking care of agent's passed status
   * @param client that send the new status
   * @param message message containing the status
   */
  public void receivedAgentStatus(Client client, Message message) {
    switch (message.getType()) {
      case COBRA_AGENT_INITIALIZED:
        addInitializedAgent(client);
        break;
      case COBRA_NEW_TRAJECTORY:
        updateAgentTrajectory(client, message);
        break;
      case COBRA_TOKEN:
        parseToken(client, message);
        break;
      case COBRA_TOKEN_REQUEST:
        parseTokenRequest(client, message);
        break;
      default:
        LOGGER.warning("CobraServer recieved status message with unknown message type");
    }
  }

  /**
   * Logs the agents that sent the initialized message. Starts the simmulation, if all the agents replied
   * @param client Client that sent the initialized message
   */
  public void addInitializedAgent(Client client) {
    initializedAgents.add(client.getUuid().toString());
    if(initializedAgents.size() == agentClients.size()) {
      startSimulation();
    }
  }

  /**
   * Returns an agent bound to a client
   * @param client Client that runs an agent
   * @return A server's local copy of Agent that is controlled by the client
   */
  public Agent getAgentByClient(Client client) {
    for (Pair<Client, Agent> acPair : agentClients.values()) {
      if(client.equals(acPair.getFirst()))
        return acPair.getSecond();
    }
    return null;
  }

  public List<Agent> getAgents() {
    List<Agent> agents = new ArrayList<>();
    for (Pair<Client, Agent> acPair : agentClients.values()) {
      if(acPair.getSecond() == null) continue;
      agents.add(acPair.getSecond());
    }
    return agents;
  }

  /**
   * Clear tasks and token to get rid of residual information from previous simulation run
   */
  public void stopSimulation() {
    CurrentTasks.clearTasks();
    Token.setAllRegions(new HashMap<>());
  }

  @Override
  public String getProblemFileContent() {
    try {
      return FileUtils.readFileToString(problemFile);
    } catch (IOException e) {
      LOGGER.severe("Could not load content of problem file to pass to nodes!");
      return null;
    }
  }

  /**
   * Updates the trajectory of local copy of an agent used for the visualization, as passed by token
   * @param client
   * @param message
   */
  public void updateAgentTrajectory(Client client, Message message) {
    Agent agent = getAgentByClient(client);
    if(agent == null) {
      return;
    }
    try{
      NewTrajectoryNotification newTrajectoryNotification = gson.fromJson(message.getContent(), NewTrajectoryNotification.class);
      agent.setTrajectory(newTrajectoryNotification.getTrajectory());
      // Sets the task based on the trajectory
      registerTaskFromTrajectory(agent, newTrajectoryNotification.getTrajectory());
    } catch (RuntimeException e) {
      LOGGER.severe("An error occured during updatind agent's trajectory: " + e.getMessage());
    }
  }

  private void parseTokenRequest(Client client, Message message) {
    synchronized (CobraDirector.class) {
      if(!tokenPassing) {
        TokenRequest request = gson.fromJson(message.getContent(), TokenRequest.class);
        Client nextHolder = clients.get(request.getRequesterId());
        TokenDto newToken = TokenDto.createFromRegions(request.getRequesterId(), Token.getAllRegions(), params.random);
        nextHolder.send(new Message("server", nextHolder.getUuid().toString(), gson.toJson(newToken), MessageType.COBRA_TOKEN));
        tokenPassing = true;
        LOGGER.info("Sending first token to agent " + request.getRequesterId());
      } else {
        LOGGER.info("Agent " + client.toString() + " asked for first token but the token is already passing");
      }
    }
  }

  /**
   * A method taking care of the token passed from server
   * @param client Client that sent the token
   * @param message Message containing the token information
   */
  private void parseToken(Client client, Message message) {
    TokenDto tokenDto = gson.fromJson(message.getContent(), TokenDto.class);
    Token.setAllRegions(tokenDto.getAsRegions()); // Update local token according to last addition

    // Only agent that could have had the trajectory changed is the one that sent the token
    Agent agent = getAgentByClient(client);
    if(tokenDto.getTrajectories().containsKey(agent.getName())) {
      BasicSegmentedTrajectory trajectory = tokenDto.getTrajectories().get(agent.getName()).getTrajectory();
      agent.setTrajectory(trajectory);
      registerTaskFromTrajectory(agent, trajectory);
    }

    // Send token only if the next recipient is different from the sender
    if(tokenDto.getNextHolderId() != Integer.parseInt(agent.getName())) {
      LOGGER.info("Sending token to " + tokenDto.getNextHolderId());
      Client nextHolder = clients.get(tokenDto.getNextHolderId());
      nextHolder.send(new Message("server", nextHolder.getUuid().toString(), gson.toJson(tokenDto), MessageType.COBRA_TOKEN));
    }
  }

  /**
   * A method extracting a target task point from trajectory
   * @param agent that changed the trajectory
   * @param trajectory the new trajectory of the agent
   */
  protected void registerTaskFromTrajectory(Agent agent, BasicSegmentedTrajectory trajectory) {
    List<Straight> segments = trajectory.getSegments();
    Straight lastSegment = segments.get(segments.size() - 1);
    // Register task based on the trajectory's last segment
    CurrentTasks.tryToRegisterTask(agent.getName(), lastSegment.getEnd().getPosition());
  }
}