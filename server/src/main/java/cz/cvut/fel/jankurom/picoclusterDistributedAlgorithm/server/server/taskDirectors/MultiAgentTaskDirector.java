package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.taskDirectors;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;


/**
 * Interface of Task directors for agents problems
 */
public interface MultiAgentTaskDirector {

    /**
     * Start the simulation
     */
    void startSimulation();

    /**
     * @return true if the size of clients assigned to this task is enough to solve the problem
     */
    boolean enoughClientsRegistered();

    /**
     * @return number of agents required to solve this problem
     */
    int getClientsRequired();
    /**
     * Handler taking care of agent's passed status
     * @param client that send the new status
     * @param message message containing the status
     */
    void receivedAgentStatus(Client client, Message message);


    /**
     * Logs the agents that sent the initialized message. Starts the simmulation, if all the agents replied
     * @param client Client that sent the initialized message
     */
    void addInitializedAgent(Client client);

    /**
     * Clear tasks and token to get rid of residual information from previous simulation run
     */
    void stopSimulation();

    /**
     * Gets a .xml file describing current problem as serielized File
     * @return
     */
    String getProblemFileContent();

}
