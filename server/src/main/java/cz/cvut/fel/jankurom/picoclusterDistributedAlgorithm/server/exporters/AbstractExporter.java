package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exporters;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Task;
import java.io.File;
import java.io.IOException;

/**
 * Template for all exporters.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public abstract class AbstractExporter {

  protected final String fileExtension;

  protected AbstractExporter(String fileExtension) {
    this.fileExtension = fileExtension;
  }

  /**
   * Returns file extension supported by the exporter.
   *
   * @return Handled file extension
   */
  public String getFileExtension() {
    return fileExtension;
  }

  /**
   * Exports tasks to a given file.
   *
   * @param tasks Tasks to export.
   * @param file  File to export to.
   * @throws IOException When something goes wrong with export.
   */
  public abstract void export(Task[] tasks, File file) throws IOException;
}
