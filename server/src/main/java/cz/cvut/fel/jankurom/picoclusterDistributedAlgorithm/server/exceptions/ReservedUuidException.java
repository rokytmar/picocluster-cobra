package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions;

/**
 * Exception thrown when node requests reserved UUID.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class ReservedUuidException extends Exception {
  public ReservedUuidException() {
  }

  public ReservedUuidException(String message) {
    super(message);
  }
}
