package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.distributor;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.DistributorRequest;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.DistributorResponse;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.NoDataServerIsRunning;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.util.logging.Logger;

/**
 * Server that server data to clients. Runs in a separate threads.
 */
public class DistributorServer extends Thread {

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");
  private final Gson gson;
  private final ServerSocket socket;
  private boolean run;

  public DistributorServer() throws IOException {
    gson = new Gson();
    socket = new ServerSocket(12346);
    socket.setSoTimeout(500);
  }

  @Override
  public void run() {
    run = true;

    while (run) {
      try {
        var client = socket.accept();
        var handler = new Thread(() -> {
          try (
              var in = new BufferedReader(new InputStreamReader(client.getInputStream()));
              var out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()))
          ) {
            var line = in.readLine();
            LOGGER.info(() -> "Received: " + line);
            var request = (DistributorRequest) gson.fromJson(line, DistributorRequest.class);
            var file = Distributor.getDistributor().getDataSource(request.getKey());
            if (file != null) {
              out.write(gson.toJson(new DistributorResponse(false, new String(Files.readAllBytes(file.toPath())))) + "\n");
            } else {
              out.write(gson.toJson(new DistributorResponse(true, "Requested data does not exist!")) + "\n");
            }
            out.flush();
            client.close();
          } catch (IOException ex) {

          } catch (NoDataServerIsRunning ex) {

          }
        });
        handler.start();
      } catch (SocketTimeoutException ex) {
        // This is ignored
      } catch (IOException e) {
        e.printStackTrace();
        break;
      }
    }
  }

  public void terminate() {
    run = false;
    try {
      join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}