package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.exceptions.ConfigurationException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class handling configuration of the server.
 *
 * @author Roman Janků
 */
public class Configuration {

  /**
   * Logger used for logging events happening in the app.
   */
  private static final Logger LOGGER = Logger.getLogger("picocluster-server");

  /**
   * HashMap used for storing the key-value pairs of configuration.
   */
  private static final HashMap<String, String> CONFIG = new HashMap<>();

  static {
    LOGGER.setLevel(Level.CONFIG);
  }

  /**
   * Loads configuration from configuration file.
   */
  public static void load() throws ConfigurationException {
    //  try reading configuration file
    try (BufferedReader br = new BufferedReader(new FileReader("project/picocluster/server/config"))) {
      LOGGER.info(() -> "Opened configuration file.");
      //  counting lines for references in error messages
      String line;
      int count = 0;
      //  reading line by line the file
      while ((line = br.readLine()) != null) {
        count++;
        int finalCount2 = count;
        LOGGER.config(() -> "Reading line " + finalCount2 + ".");
        if (line.trim().startsWith("#")) {
          // commented line, just ignoring this
          int finalCount1 = count;
          LOGGER.config(() -> "Ignored line " + finalCount1 + ".");
        } else if (line.trim().length() > 2 && line.matches(".+=.+")) {
          //  correctly formed line with key-value pair
          String[] pair = line.trim().split("=");
          CONFIG.put(pair[0], pair[1]);
          LOGGER.config(() -> "Inserted pair '" + pair[0] + "' = '" + pair[1] + "' into config.");
        } else {
          //  malformed line
          int finalCount = count;
          String finalLine = line;
          LOGGER.warning(() -> "Malformed line in configuration file: [" + finalCount + "] " + finalLine);
        }
      }
    } catch (FileNotFoundException ex) {
      //  Configuration file was not found
      LOGGER.severe(() -> "Configuration file not found. Make sure it is named 'config' and placed at the same directory you are running your java command.");
      throw new ConfigurationException("Configuration file not found.");
    } catch (IOException ex) {
      //  Other exception when reading configuration file
      LOGGER.severe(() -> "Exception occurred when reading configuration file. Message: " + ex.getMessage());
      throw new ConfigurationException(ex.getMessage());
    }
  }

  /**
   * Returns value for a given key. If the given is not in the configuration null is returned.
   *
   * @param key Key to look for in configuration.
   * @return Value for a given key or null if key was not found.
   */
  public static String getValue(String key) {
    return CONFIG.get(key);
  }

  /**
   * Tells weather key exists in configuration.
   *
   * @param key Key to look for.
   * @return True if key exists in config, false if key doesn't exist in config.
   */
  public static boolean hasKey(String key) {
    return CONFIG.containsKey(key);
  }
}