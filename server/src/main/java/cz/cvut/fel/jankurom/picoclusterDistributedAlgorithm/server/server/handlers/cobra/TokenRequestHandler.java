package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.AbstractHandler;

public class TokenRequestHandler extends AbstractHandler {

    public TokenRequestHandler() {
        this.handledMessageType = MessageType.COBRA_TOKEN_REQUEST;
    }
    @Override
    public void handleMessage(Message message, Client client) {
        Server.getInstance().receivedStatus(client, message);
    }

}
