package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Client;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.Server;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.server.server.handlers.AbstractHandler;

public class ADPPInnerMessageHandler extends AbstractHandler {

    public ADPPInnerMessageHandler() {
        this.handledMessageType = MessageType.ADPP_INNER_MESSAGE;
    }

    @Override
    public void handleMessage(Message message, Client client) {
        Server.getInstance().receivedStatus(client, message);
    }
}
