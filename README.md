Bachelor thesis - Picocluster submodule
==============================

**Name:** Experimental validation of distributed algorithms
**Author:** Martin Rokyta (rokytmar@fel.cvut.cz)
**Supervisor:** doc. Ing. Jiří Vokřínek, Ph.D. (jiri.vokrinek@fel.cvut.cz)
**Department of supervisor:** Department of Computer Science

About
------
This repository includes a customized fork of the Picocluster project. The project was enhanced in order to run specialized multi-agent path-planning algorithms.
Running the project using this repository alone is therefore not encouraged. Refer to the [parent project](https://gitlab.fel.cvut.cz/rokytmar/bachelor-thesis) to find out how to package and start the project.
