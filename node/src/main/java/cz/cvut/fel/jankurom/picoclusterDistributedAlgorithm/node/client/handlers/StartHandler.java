package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.StartTask;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * Handler for handling START messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class StartHandler extends AbstractHandler {

  /**
   * Gson for parsing JSON into Java objects.
   */
  private final Gson gson;

  /**
   * Create a new StartHandler.
   */
  public StartHandler() {
    handledMessageType = MessageType.START;
    gson = new Gson();
  }

  @Override
  public void handleMessage(Message message) {
    StartTask taskData = gson.fromJson(message.getContent(), StartTask.class);
    try {
      Main.getClient().setCoworkers(taskData.getNetwork().toArray(new String[0]));
      Main.getClient().runTask((AbstractAlgorithm) Class.forName(taskData.getTaskName()).getDeclaredConstructor().newInstance(), taskData.getParameters().toArray(String[]::new));
    } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
      LOGGER.warning(ex + " : " + ex.getMessage());
      Arrays.stream(ex.getStackTrace()).forEach(l -> LOGGER.warning(l.toString()));
      Main.getClient().clearCoworkers();
    }
  }
}