package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;
import java.awt.Color;

public interface TaskListener {
  /**
   * Called by algorithm when it is finished.
   *
   * @param result Partial result of the algorithm.
   */
  void taskFinished(String result);

  /**
   * Called by algorithm when it failed.
   *
   * @param reason Reason for the algorithm failure.
   */
  void taskFailed(String reason);

  /**
   * Called by algorithm when it wants to send status info to the server.
   *
   * @param content Status information.
   */
  void taskStatusUpdate(String content);

  /**
   * Called by algorithm when it wants to send info specific message type
   * @param message Status information
   */
  void taskStatusUpdate(String content, MessageType messageType);

  /**
   * Used to set specific LED to specific color.
   *
   * @param index Index of the LED.
   * @param color Desired color.
   */
  void setLed(int index, Color color);

  /**
   * Used to set specific LED to specific color using RGB values. These values should be between 0 and 255.
   *
   * @param index Index of the LED.
   * @param red   Red part of the color.
   * @param green Green part of the color.
   * @param blue  Blue part of the color.
   */
  void setLed(int index, int red, int green, int blue);

  /**
   * Called whet the task had stopped.
   */
  void taskStopped();

  /**
   * Used to retrieve list of coworkers on current task.
   *
   * @return List of coworkers UUIDs.
   */
  String[] getCoworkers();

  /**
   * Used to sent message over UDP to another node. The delivery nor correctness is guaranteed.
   *
   * @param uuid    UUID of the receiver.
   * @param content Content of the message.
   * @throws TooBigMessage When the message is too big.
   */
  void sendMessageToNode(String uuid, String content) throws TooBigMessage;

  /**
   * Used to download distributed data from the server.
   *
   * @param identifier Identifier of the data.
   * @return Downloaded data.
   */
  String fetchData(String identifier);
}