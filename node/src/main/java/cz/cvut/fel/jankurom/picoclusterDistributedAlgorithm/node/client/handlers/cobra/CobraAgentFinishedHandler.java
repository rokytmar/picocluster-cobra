package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.AbstractHandler;

/**
 * A handler specifying what to do with the AgentFinishedNotification message
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraAgentFinishedHandler extends AbstractHandler {
  public CobraAgentFinishedHandler() {
    this.handledMessageType = MessageType.COBRA_AGENT_FINISHED;
  }
  @Override
  public void handleMessage(Message message) {
    Main.getClient().receivedDTOMessageFromServer(message);
  }
}
