package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.cobra;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.AbstractHandler;

/**
 * A handler specifying what to do with the CobraAgentStarted message
 * @author Martin Rokyta (rokytmar@fel.cvut.cz)
 */
public class CobraStartAgentHandler extends AbstractHandler {
  public CobraStartAgentHandler() {
    handledMessageType = MessageType.COBRA_START_AGENT;
  }

  @Override
  public void handleMessage(Message message) {
    Main.getClient().receivedDTOMessageFromServer(message);
  }
}
