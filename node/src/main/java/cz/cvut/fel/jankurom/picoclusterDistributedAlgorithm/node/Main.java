package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.BlinkIt;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.Client;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

/**
 * Main class of the node application with main method.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class Main {
  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  // Default fields that are used only for local runs (If ON_CLUSTER system environment is not set)
  private static final InetAddress SERVER_ADDRESS = getLanServerAddress();

  static InetAddress getLanServerAddress() {
    try {
      return InetAddress.getByAddress(new byte[] {(byte)127, (byte)0, (byte) 0, (byte) 1});
    } catch (UnknownHostException e) {
      e.printStackTrace();
      return null;
    }
  }

  private static final int SERVER_PORT = 12345;
  private static final int UDP_MAX_PACKET_SIZE = 1100000;
  private static final boolean NOTIFY_SERVER = false;

  private static InetAddress serverAddress;
  private static int serverPort;
  private static int udpMaxPacketSize;
  private static boolean notifyServer;

  /**
   * Client that is connected to server.
   */
  private static Client client;

  private Main() {

  }

  /**
   * Main method of the node application.
   *
   * @param args Parameters from terminal.
   */
  public static void main(String... args) {
    String onClusterString = System.getenv("ON_CLUSTER");
    boolean onCluster = onClusterString != null && onClusterString.equals("true");
    BlinkIt.blinkItSupported = onCluster;
    BlinkIt.initialise();

    Runtime.getRuntime().addShutdownHook(new Thread(() -> BlinkIt.setTerminated()));

    LOGGER.info(() -> "Node application is starting ...");
    try {
      serverAddress = System.getenv("PICOCLUSTER_SERVER") != null ?
          InetAddress.getByName(System.getenv("PICOCLUSTER_SERVER").split(":")[0]) :
          SERVER_ADDRESS;
      serverPort = System.getenv("PICOCLUSTER_SERVER") != null ? Integer.parseInt(System.getenv("PICOCLUSTER_SERVER").split(":")[1]) :
          SERVER_PORT;
      udpMaxPacketSize = System.getenv("UDP_PACKET_SIZE") != null ? Integer.parseInt(System.getenv("UDP_PACKET_SIZE")) : UDP_MAX_PACKET_SIZE;
      notifyServer = System.getenv("END_SENT_MESSAGE_INFO") != null ? (System.getenv("END_SENT_MESSAGE_INFO").equalsIgnoreCase("true")) : NOTIFY_SERVER;

      client = new Client(onCluster);
      client.start();
      BlinkIt.initialise();
      LOGGER.info(() -> "Node application has successfully started.");
    } catch (IOException ex) {
      LOGGER.severe(() -> "Problem happened when starting node. Will terminate. Cause: " + ex.getMessage());
    }
  }

  /**
   * Parses the address of the server from environmental variable containing server's IP address and port.
   *
   * @return Parsed IP address.
   * @throws UnknownHostException Thrown when IP address is unknown.
   */
  public static InetAddress getServerAddress() throws UnknownHostException {
    return serverAddress;
  }

  /**
   * Parses the port number of the server from environmental variable containing server's IP address and port.
   *
   * @return Parsed server port number.
   */
  public static int getServerPort() {
    return serverPort;
  }

  /**
   * Returns the maximum size of UDP packet in bytes.
   * @return Maximum size of UDP packet.
   */
  public static int getMaxUdpPacketSize() {
    return udpMaxPacketSize;
  }

  /**
   * Tells weather server should be notified when message is sent on UDP.
   * @return Notify server ond UDP sent.
   */
  public static boolean notifyServerOnMessageSent() {
    return notifyServer;
  }

  /**
   * Returns the Client class created by main method.
   *
   * @return Client class created by main method.
   */
  public static Client getClient() {
    return client;
  }
}