package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.AbstractAlgorithm;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.algorithms.TaskRunner;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.DistributorRequest;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.DistributorResponse;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.NodeDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.NoHandlerForMessageTypeException;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.exceptions.TooBigMessage;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.HandlersAccessor;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;

/**
 * Class handling communication with server.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class Client extends Thread implements TaskListener, UdpListener {

  private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  private static final int MAX_MSG_DISPLAY_LENGTH = 100;

  /**
   * Socket for communication with client.
   */
  private final Socket socket;
  /**
   * Gson for parsing JSON into Java objects and serializing Java objects into JSON.
   */
  private final Gson gson;
  /**
   * List of all nodes on the server.
   */
  private final List<NodeDto> nodes;
  private final UdpSocket udpSocket;
  /**
   * Input buffered reader for reading data from server.
   */
  private BufferedReader in;
  /**
   * Output buffered writer for writing data to server.
   */
  private BufferedWriter out;
  /**
   * Variable telling weather the client is running or not.
   */
  private boolean run;
  /**
   * UUID of the node.
   */
  private UUID uuid;
  /**
   * Current task. Is null when node has no task.
   */
  private Task task;
  private String[] coworkers;

  /**
   * Flag whether the Client is running on separate Raspi cluster, or local machine
   */
  private boolean onCluster;

  /**
   * Creates a new client and connects to a server.
   *
   * @throws IOException When there is a connection problem.
   * @param onCluster flag whether the client is running on a cluster or local machine
   */
  public Client(boolean onCluster) throws IOException {
    LOGGER.info(() -> {
      try {
        return "Connecting to server on " + Main.getServerAddress().getHostAddress() + ":" + Main.getServerPort();
      } catch (UnknownHostException e) {
        LOGGER.severe(() -> "Could not connect to server!");
      }
      return ""; //  Shouldn't really fall here
    });

    socket = new Socket(Main.getServerAddress().getHostAddress(), Main.getServerPort());
    socket.setSoTimeout(2500);
    gson = new Gson();
    run = true;
    uuid = null;
    nodes = new ArrayList<>();
    coworkers = null;

    udpSocket = new UdpSocket(this, socket.getLocalPort(), onCluster);
    udpSocket.start();
  }

  @Override
  public void run() {
    //  Initialisation
    try {
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    } catch (IOException ex) {
      LOGGER.severe(() -> "Could not open input/output streams. Will terminate. Cause: " + ex.getMessage());
      return;
    }
    BlinkIt.setConnected();
    sendUuidRequest();

    //  Main loop reading messages
    while (run) {
      Message message = null;
      try {
        var line = in.readLine();
        if (line == null) {
          LOGGER.info(() -> "Disconnected from server.");
          break;
        }
        LOGGER.info(() -> "Received: " + (line.length() > MAX_MSG_DISPLAY_LENGTH ?
            "Message too long, displaying only first " + MAX_MSG_DISPLAY_LENGTH + " chars: " + line.substring(0, MAX_MSG_DISPLAY_LENGTH) : line));
        message = gson.fromJson(line, Message.class);
        HandlersAccessor.getHandlerForMessageType(message.getType()).handleMessage(message);
      } catch (SocketTimeoutException e) {
        //  This is ignored and is implemented only to gracefully shut down
        LOGGER.config(() -> "Timeout.");
      } catch (IOException ex) {
        LOGGER.severe(() -> "Error occurred when communicating with server. Will terminate. Cause: " + ex.getMessage());
        run = false;
      } catch (NoHandlerForMessageTypeException ex) {
        Message finalMessage = message;
        LOGGER.warning(() -> "Received message with unknown type from '" + finalMessage.getFrom() + "'. Cause: " + ex.getMessage());
        try {
          sendMessage(new Message(uuid.toString(), message.getFrom(), ex.getMessage(), MessageType.ERROR));
        } catch (IOException e) {
          LOGGER.severe(() -> "Could not send error message to server. Cause: " + e.getMessage());
        }
      }
    }

    LOGGER.info(() -> "Terminating ...");
    stopTask();
    try {
      in.close();
    } catch (IOException ex) {
      LOGGER.warning(() -> "Could not close input reader. Cause: " + ex.getMessage());
    }
    try {
      out.close();
    } catch (IOException ex) {
      LOGGER.warning(() -> "Could not close output reader. Cause: " + ex.getMessage());
    }
    try {
      socket.close();
    } catch (IOException ex) {
      LOGGER.warning(() -> "Could not close socket. Cause: " + ex.getMessage());
    }
    udpSocket.terminate();
    LOGGER.info(() -> "Terminated.");
  }

  /**
   * Method for shutting down the client gracefully.
   */
  public void shutDown() {
    run = false;
    try {
      join();
    } catch (InterruptedException ex) {
      LOGGER.warning(() -> "Could not terminate properly. Will terminate anyway. Cause: " + ex.getMessage());
    }
  }

  @Override
  public String[] getCoworkers() {
    return coworkers;
  }

  /**
   * Method for setting up list of coworkers on the task.
   *
   * @param coworkers List of coworkers.
   */
  public void setCoworkers(String[] coworkers) {
    this.coworkers = Arrays.stream(coworkers).filter(c -> !c.equals(uuid.toString())).toArray(String[]::new);
  }

  @Override
  public void sendMessageToNode(String uuid, String content) throws TooBigMessage {
    var target = nodes.stream().filter(n -> n.getUuid().toString().equals(uuid)).findFirst();
    if (target.isPresent()) {
      var buffer = (getUuid().toString() + ":" + content).getBytes(StandardCharsets.UTF_8); // Send uuid along with message
      if (buffer.length > Main.getMaxUdpPacketSize()) {
        throw new TooBigMessage();
      }
      var packet = new DatagramPacket(buffer, buffer.length, target.get().getAddress(), target.get().getUDPPort());
      LOGGER.info(() -> "Sent '" + content + "' to " + packet.getAddress() + ":" + packet.getPort() + " from " + socket.getInetAddress() + ":" + socket.getLocalPort());
      try (var socket = new DatagramSocket()) {
        socket.send(packet);
        if (Main.notifyServerOnMessageSent()) {
          sendMessage(new Message(this.uuid.toString(), "server", uuid, MessageType.SENT));
        }
      } catch (IOException ex) {
        LOGGER.severe("Could not sent message to node! Reason: " + ex.getMessage());
      }
    }
  }

  /**
   * Method used to clear the list of coworkers.
   */
  public void clearCoworkers() {
    coworkers = null;
  }

  /**
   * Sends UUID request to a server.
   */
  private void sendUuidRequest() {
    Message message = new Message("", "server", UUID.randomUUID().toString() + "/" + udpSocket.getPort(), MessageType.UUID_REQUEST);
    try {
      sendMessage(message);
      LOGGER.info(() -> "Send UUID_REQUEST with UUID/port: " + message.getContent());
    } catch (IOException ex) {
      run = false;
      LOGGER.severe(() -> "Could not send UUID_REQUEST. Will terminate. Cause: " + ex.getMessage());
    }
  }

  /**
   * Returns the client's UUID.
   *
   * @return Client's UUID.
   */
  public UUID getUuid() {
    return uuid;
  }

  /**
   * Sets client's UUID.
   *
   * @param uuid New client's UUID.
   */
  public void setUuid(UUID uuid) {
    this.uuid = uuid;
    BlinkIt.setReady();
  }

  /**
   * Tells weather client has UUID.
   *
   * @return TRUE if client has UUID, false if not.
   */
  public boolean hasUuid() {
    return uuid != null;
  }

  /**
   * Starts a new task. New task is not started if there is a task already running.
   *
   * @param algorithm  Algorithm to run.
   * @param parameters Parameters for the algorithm.
   */
  public void runTask(AbstractAlgorithm algorithm, String... parameters) {
    LOGGER.info(() -> "Running task " + algorithm.name() + ".");
    if (hasTask()) {
      return;
    }
    task = new Task(this, algorithm, parameters);
    task.start();
    BlinkIt.setComputing();
  }

  /**
   * Called to stop a currently running task. Does nothing if there is no task.
   */
  public void stopTask() {
    if (task != null) {
      task.stopTask();
    } else {
      taskStopped();
    }
  }

  @Override
  public void taskFinished(String result) {
    Message message = new Message(uuid.toString(), "server", result, MessageType.FINISHED);
    try {
      sendMessage(message);
    } catch (IOException ex) {
      LOGGER.severe(() -> "Could not send task result to server. Cause: " + ex.getMessage());
    }
    task = null;
    BlinkIt.setFinished();
    coworkers = null;
  }

  @Override
  public void taskFailed(String reason) {
    Message message = new Message(uuid.toString(), "server", reason, MessageType.FAILED);
    try {
      sendMessage(message);
    } catch (IOException ex) {
      LOGGER.severe(() -> "Could not send task result to server. Cause: " + ex.getMessage());
    }
    task = null;
    BlinkIt.setFailed();
    coworkers = null;
  }

  @Override
  public void taskStatusUpdate(String content) {
    taskStatusUpdate(content, MessageType.STATUS);
  }

  @Override
  public void taskStatusUpdate(String content, MessageType messageType) {
    Message message = new Message(uuid.toString(), "server", content, messageType);
    try {
      sendMessage(message);
    } catch (IOException ex) {
      LOGGER.severe(() -> "Could not send task result to server. Cause: " + ex.getMessage());
    }
  }

  @Override
  public void setLed(int index, Color color) {
    BlinkIt.setCustom(index, color);
  }

  @Override
  public void setLed(int index, int red, int green, int blue) {
    BlinkIt.setCustom(index, red, green, blue);
  }

  @Override
  public void taskStopped() {
    task = null;
    BlinkIt.setStopped();
    try {
      sendMessage(new Message(uuid.toString(), "server", "", MessageType.STOPPED));
    } catch (IOException ex) {
      LOGGER.severe(() -> "Could not send task result to server. Cause: " + ex.getMessage());
    }
  }

  @Override
  public String fetchData(String identifier) {
    DistributorResponse data;
    try (
        var dataSocket = new Socket(Main.getServerAddress(), Main.getServerPort() + 1);
        var in = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
        var out = new BufferedWriter(new OutputStreamWriter(dataSocket.getOutputStream()))
    ) {
      var req = gson.toJson(new DistributorRequest(identifier));
      LOGGER.info("Sent: " + req);
      out.write(req + "\n");
      out.flush();
      data = gson.fromJson(in.readLine(), DistributorResponse.class);
    } catch (UnknownHostException e) {
      return null;
    } catch (IOException e) {
      return null;
    }
    if (data != null && !data.isError()) {
      return data.getData();
    }
    return null;
  }

  /**
   * Tells weather there is a task running.
   *
   * @return TRUE if there is task running, FALSE otherwise.
   */
  public boolean hasTask() {
    return task != null;
  }

  /**
   * Sends message to server.
   *
   * @param message Message to send.
   * @throws IOException Thrown when error happens while sending data to server.
   */
  public void sendMessage(Message message) throws IOException {
    out.write(gson.toJson(message) + "\n");
    out.flush();
  }

  /**
   * Returns list of all nodes connected to the client that are ready.
   *
   * @return List of all ready nodes.
   */
  public synchronized List<NodeDto> getListOfConnectedNodes() {
    return nodes;
  }

  /**
   * Updates list of ready nodes.
   *
   * @param nodes List of all connected nodes.
   */
  public synchronized void updateNodesList(List<NodeDto> nodes) {
    this.nodes.clear();
    nodes.stream()
        .filter(n -> !n.getUuid().equals(uuid))
        .forEach(n -> this.nodes.add(n));
    LOGGER.info(() -> "Updated list of ready nodes. Current is: " + this.nodes);
  }

  public void receivedDTOMessageFromServer(Message message) {
      task.algorithm.receivedDTOMessage(message);
  }

  @Override
  public void receivedMessage(InetAddress address, int port, String message) {
    if (task == null) {
      return;
    }

    String content;
    Optional<NodeDto> sender;

    int dividerPos = message.indexOf(':');
    if(dividerPos != -1) {
      String senderUUID = message.substring(0, dividerPos);
      content = message.substring(dividerPos + 1);
      sender = nodes.stream().filter(n -> n.getUuid().toString().equals(senderUUID)).findFirst();
    } else {
      content = message;
      sender = nodes.stream().filter(n -> n.getAddress().equals(address)).findFirst();
    }
    sender.ifPresent(nodeDto -> task.receivedMessage(nodeDto.getUuid().toString(), content, "UDP"));
  }

  /**
   * Internal representation of node.
   */
  private static class Task extends Thread implements TaskRunner {

    private final TaskListener listener;
    private final AbstractAlgorithm algorithm;
    private final String[] parameters;
    private boolean stop;

    Task(TaskListener listener, AbstractAlgorithm algorithm, String... parameters) {
      this.algorithm = algorithm;
      this.listener = listener;
      this.parameters = parameters;
      this.stop = false;
    }

    public void receivedMessage(String uuid, String content, String protocol) {
      algorithm.receivedMessage(uuid, content);
      LOGGER.info(() -> "Received from " + uuid + " on " + protocol + ".");
    }

    @Override
    public String[] getCoworkers() {
      return listener.getCoworkers();
    }

    @Override
    public void sendMessageToNode(String uuid, String content) throws TooBigMessage {
      listener.sendMessageToNode(uuid, content);
      LOGGER.info(() -> "Sent to " + uuid + " on UDP.");
    }

    @Override
    public void run() {
      try {
        String result = algorithm.run(parameters, this);
        if (!stop) {
          LOGGER.info(() -> "Finished algorithm '" + algorithm.getClass().getName() + "' with result: " + result);
          listener.taskFinished(result);
        }
      } catch (Exception ex) {
        LOGGER.severe(() -> "Assigned task failed. Cause: " + ex.getMessage());
        listener.taskFailed(ex.getMessage());
      }
    }

    /**
     * Called to stop currently running task. Does nothing if there is no task running.
     */
    public void stopTask() {
      stop = true;
      algorithm.stop();
    }

    @Override
    public void sendStatusUpdate(String status) {
      listener.taskStatusUpdate(status);
    }

    @Override
    public void sendStatusUpdate(String status, MessageType messageType) {
      listener.taskStatusUpdate(status, messageType);
    }


    @Override
    public void setLed(int index, Color color) {
      listener.setLed(index, color);
    }

    @Override
    public void setLed(int index, int red, int green, int blue) {
      listener.setLed(index, red, green, blue);
    }

    @Override
    public void taskStopped() {
      listener.taskStopped();
    }

    @Override
    public String fetchData(String identifier) {
      return listener.fetchData(identifier);
    }

    @Override
    public void triggerMessageQueueProcessing() {
      algorithm.triggerMessageQueueProcessing();
    }
  }

  /**
   * Thread listening on UDP socket for incomming messages.
   */
  private static class UdpSocket extends Thread {
    private final int port;
    private DatagramSocket socket;
    private volatile boolean run;
    private UdpListener listener;

    UdpSocket(UdpListener listener, int clientTcpPort, boolean onCluster) throws SocketException {
      // Sets udp port to server port + 2 (default for cluster) or client's tcp socket + 1000 for local testing
      port = onCluster ? Main.getServerPort() + 2 : clientTcpPort + 1000;
      socket = new DatagramSocket(port);
      LOGGER.info(() -> "Started udp on " + socket.getLocalAddress().toString() + ":" + socket.getLocalPort());
      socket.setSoTimeout(2500);
      run = true;
      this.listener = listener;
    }

    /**
     * Stops the thread.
     */
    public void terminate() {
      run = false;
    }

    public void run() {
      while (run) {
        var buffer = new byte[Main.getMaxUdpPacketSize()];
        var packet = new DatagramPacket(buffer, buffer.length);
        try {
          socket.receive(packet);
          var received = new String(packet.getData(), 0, packet.getLength());
          LOGGER.info("Received from " + packet.getAddress() + ":" + packet.getPort() + ": " + received);
          listener.receivedMessage(packet.getAddress(), packet.getPort(), received);
        } catch (SocketTimeoutException ex) {
          LOGGER.info(() -> "UDP timeout");
        } catch (IOException ex) {
          break;
        }
      }
      socket.close();
    }

    public int getPort() {
      return port;
    }
  }
}