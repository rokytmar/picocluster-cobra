package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client;

import java.awt.Color;
import jimbo.pijava.blinkt.BlinktController;

/**
 * Controller for Blinkt! LEDs
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public final class BlinkIt {
  // Flag stating whether to use the module.
  // If set to false, no calls for the module will be sent from the node, so that it can be run on a platform without the module
  public static boolean blinkItSupported;

  private static BlinktController blinkIt = getBlinkIt();
  private static Color[] backup;
  private static volatile boolean custom;


  static BlinktController getBlinkIt() {
    if(blinkItSupported){
      return new BlinktController();
    }
    return null;
  }

  private BlinkIt() {

  }

  /**
   * Initializes the LEDs to initial state.
   */
  public static void initialise() {
    if(!blinkItSupported) return;
    if(blinkIt == null) {
      blinkIt = new BlinktController();
    }
    backup = new Color[8];
    custom = false;
    blinkIt.clear();
    blinkIt.setBrightness(.1F);
    blinkIt.set(0, Color.GREEN);
    backup[0] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to connected state.
   */
  public static void setConnected() {
    if(!blinkItSupported) return;
    restore();
    for (int i = 2; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(1, Color.GREEN);
    backup[1] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to ready state.
   */
  public static void setReady() {
    if(!blinkItSupported) return;
    restore();
    for (int i = 3; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(2, Color.GREEN);
    backup[2] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to computing state.
   */
  public static void setComputing() {
    if(!blinkItSupported) return;
    restore();
    turnOf(5);
    turnOf(6);
    blinkIt.set(7, Color.YELLOW);
    backup[7] = Color.YELLOW;
    blinkIt.push();
  }

  /**
   * Sets LEDs top finished state.
   */
  public static void setFinished() {
    if(!blinkItSupported) return;
    restore();
    blinkIt.set(7, Color.GREEN);
    backup[7] = Color.GREEN;
    blinkIt.push();
  }

  /**
   * Sets LEDs to stopped state.
   */
  public static void setStopped() {
    if(!blinkItSupported) return;
    restore();
    turnOf(7);
    blinkIt.set(6, Color.RED);
    backup[6] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets LEDs to failed state.
   */
  public static void setFailed() {
    if(!blinkItSupported) return;
    restore();
    turnOf(7);
    blinkIt.set(5, Color.RED);
    backup[5] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets LEDs to disconnected state.
   */
  public static void setDisconnected() {
    if(!blinkItSupported) return;
    restore();
    for (int i = 2; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(1, Color.YELLOW);
    backup[1] = Color.YELLOW;
    blinkIt.push();
  }

  /**
   * Sets LEDs to terminated state.
   */
  public static void setTerminated() {
    if(!blinkItSupported) return;
    restore();
    for (int i = 0; i < 8; i++) {
      turnOf(i);
    }
    blinkIt.set(3, Color.RED);
    blinkIt.set(4, Color.RED);
    backup[3] = Color.RED;
    backup[4] = Color.RED;
    blinkIt.push();
  }

  /**
   * Sets selected LED to desired color.
   *
   * @param index Index of the LED.
   * @param color Color of the LED.
   */
  public static void setCustom(int index, Color color) {
    if(!blinkItSupported) return;
    backUp();
    blinkIt.set(index, color);
    blinkIt.push();
  }

  /**
   * Sets LED at selected index to custom color.
   *
   * @param index Index of the LED.
   * @param red   Red part of the color.
   * @param green Green part of the color.
   * @param blue  Blue part of the color.
   */
  public static void setCustom(int index, int red, int green, int blue) {
    if(!blinkItSupported) return;
    backUp();
    blinkIt.set(index, red, green, blue);
    blinkIt.push();
  }

  /**
   * Sets brightness of all LEDs.
   *
   * @param brightness Sets brightness of LEDs.
   */
  public static void setBrightness(float brightness) {
    if(!blinkItSupported) return;
    backUp();
    blinkIt.setBrightness(brightness);
    blinkIt.push();
  }

  private static void turnOf(int index) {
    if(!blinkItSupported) return;
    blinkIt.set(index, 0, 0, 0);
    backup[index] = null;
    blinkIt.push();
  }

  private static void backUp() {
    if(!blinkItSupported) return;
    if (custom) {
      return;
    }
    for (int i = 0; i < 8; i++) {
      blinkIt.set(i, 0, 0, 0);
    }
    custom = true;
    blinkIt.push();
  }

  private static void restore() {
    if(!blinkItSupported) return;
    if (!custom) {
      return;
    }
    blinkIt.setBrightness(.1F);
    for (int i = 0; i < 8; i++) {
      if (backup[i] == null) {
        turnOf(i);
      } else {
        blinkIt.set(i, backup[i]);
      }
    }
    custom = false;
    blinkIt.push();
  }
}