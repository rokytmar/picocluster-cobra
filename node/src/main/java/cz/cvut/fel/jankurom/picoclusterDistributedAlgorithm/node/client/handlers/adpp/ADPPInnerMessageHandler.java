package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.adpp;

import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers.AbstractHandler;

public class ADPPInnerMessageHandler extends AbstractHandler {

    public ADPPInnerMessageHandler() {
        this.handledMessageType = MessageType.ADPP_INNER_MESSAGE;
    }

    @Override
    public void handleMessage(Message message) {
        Main.getClient().receivedDTOMessageFromServer(message);
    }
}
