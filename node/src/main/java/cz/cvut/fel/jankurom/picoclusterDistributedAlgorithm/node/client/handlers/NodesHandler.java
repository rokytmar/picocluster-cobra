package cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.client.handlers;

import com.google.gson.Gson;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.Message;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.MessageType;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.library.communication.dto.NodeDto;
import cz.cvut.fel.jankurom.picoclusterDistributedAlgorithm.node.Main;
import java.util.Arrays;

/**
 * Handler for handling STOP messages.
 *
 * @author Roman Janků (jankurom@fel.cvut.cz)
 */
public class NodesHandler extends AbstractHandler {

  private final Gson gson;

  /**
   * Create a new StartHandler.
   */
  public NodesHandler() {
    handledMessageType = MessageType.NODES;
    gson = new Gson();
  }

  @Override
  public void handleMessage(Message message) {
    Main.getClient().updateNodesList(Arrays.asList(gson.fromJson(message.getContent(), NodeDto[].class)));
  }
}
